import React from 'react';
import { Easing, Dimensions } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";

import { Icon, Header, CourseHeader } from "../components";
import { Images, materialTheme } from "../constants";
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

// screens
import OnboardingScreen from "../screens/Onboarding";
import HomeScreen from "../screens/Home";
import SignInScreen from "../screens/SignIn";
import SignUpScreen from "../screens/SignUp";
import UpdateProfileScreen from "../screens/UpdateProfile";
import SubscriptionsScreen from "../screens/Subscriptions";

import ProfileScreen from "../screens/Profile";
import SettingsScreen from "../screens/Settings";
import PrivacyScreen from "../screens/Privacy";
import AboutScreen from "../screens/About";
import ContactUsScreen from "../screens/ContactUs";
import TermsOfUseScreen from "../screens/Terms0fUse";
import LegalNoticeScreen from "../screens/LegalNotice";
import MyCoursesScreen from "../screens/MyCourses";
import AvailableCoursesListScreen from "../screens/AvailableCoursesList";
import CoursesListScreen from "../screens/CoursesList";
import LearnExpressionCourseScreen from "../screens/Courses/LearnExpressionCourse";
import DialogCourseScreen from "../screens/Courses/DialogCourse";
import ChooseRightAnswerCourseScreen from "../screens/Courses/ChooseRightAnswerCourse";
import TranslateWriteExpressionWithBlockCourseScreen from "../screens/Courses/TranslateWriteExpressionWithBlockCourse";
import PairingExpressionCourseScreen from "../screens/Courses/PairingExpressionCourse";
import FillExpressionWithBlockCourseScreen from "../screens/Courses/FillExpressionWithBlockCourse";
import TranslateExpressionByWritingCourseScreen from "../screens/Courses/TranslateExpressionByWritingCourse";
import FillExpressionByWritingCourseScreen from "../screens/Courses/FillExpressionByWritingCourse";

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Context from '../store/context';

const { width, height } = Dimensions.get("screen");

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const Tab = createBottomTabNavigator();

function OnboardingStack(props) {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen
        name="Onboarding"
        component={OnboardingScreen}
      />
      <Drawer.Screen
        name="Sign In"
        component={SignInScreen}
      />
      <Drawer.Screen
        name="Sign Up"
        component={SignUpScreen}
      />
     
    </Stack.Navigator>
  );
}

function HomeStack(props) {
  const context = React.useContext(Context);
  
  let currentLanguage;
  if (context.getCurrentUser()) {
    currentLanguage = context.getCurrentUser().languages.filter(language => language.isCurrentLearning === true).shift();
  }

  return (
    <Stack.Navigator
      headerMode="screen"
    >
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          header: ({ navigation }) => (
            <Header
              title={currentLanguage !== undefined ? currentLanguage.id.title : ''}
              navigation={navigation}
              routeNameToNavigate="MyCourses"
            />
          )
        }}
      />
      <Stack.Screen
        name="MyCourses"
        component={MyCoursesScreen} 
        options={{
          header: ({ navigation }) => (
            <Header
              back
              title="Mes langues"
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="AvailableCoursesList"
        component={AvailableCoursesListScreen} 
        options={{
          header: ({ navigation }) => (
            <Header
              back
              title="Je veux apprendre..."
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="CoursesList"
        component={CoursesListScreen}
        options={({ route }) => ({
          header: ({ navigation }) => (
            <Header
              back
              title={route.params.chapterTitle}
              navigation={navigation}
            />
          )
        })}
      />
      <Stack.Screen
        name="LearnExpressionCourse"
        component={LearnExpressionCourseScreen} 
        options={{
          header: ({ navigation }) => (
            <CourseHeader
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="DialogCourse"
        component={DialogCourseScreen} 
        options={{
          header: ({ navigation }) => (
            <CourseHeader
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="ChooseRightAnswerCourse"
        component={ChooseRightAnswerCourseScreen} 
        options={{
          header: ({ navigation }) => (
            <CourseHeader
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="TranslateWriteExpressionWithBlockCourse"
        component={TranslateWriteExpressionWithBlockCourseScreen} 
        options={{
          header: ({ navigation }) => (
            <CourseHeader
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="PairingExpressionCourse"
        component={PairingExpressionCourseScreen} 
        options={{
          header: ({ navigation }) => (
            <CourseHeader
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="FillExpressionWithBlockCourse"
        component={FillExpressionWithBlockCourseScreen} 
        options={{
          header: ({ navigation }) => (
            <CourseHeader
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="TranslateExpressionByWritingCourse"
        component={TranslateExpressionByWritingCourseScreen} 
        options={{
          header: ({ navigation }) => (
            <CourseHeader
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="FillExpressionByWritingCourse"
        component={FillExpressionByWritingCourseScreen} 
        options={{
          header: ({ navigation }) => (
            <CourseHeader
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="Subscriptions"
        component={SubscriptionsScreen} 
        options={{
          header: ({ navigation, scene }) => (
            <Header
              back
              title="Premium"
              scene={scene}
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="ContactUs"
        component={ContactUsScreen} 
        options={{
          header: ({ navigation, scene }) => (
            <Header
              back
              title="Contactez-nous"
              scene={scene}
              navigation={navigation}
            />
          )
        }}
      />
    </Stack.Navigator>
  );
}

function SettingsStack(props) {
  return (
    <Stack.Navigator
      initialRouteName="Settings"
      headerMode="screen"
    >
      <Stack.Screen
        name="Settings"
        component={SettingsScreen} 
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Privacy"
        component={PrivacyScreen} 
        options={{
          header: ({ navigation, scene }) => (
            <Header
              back
              title="Politique de confidentialité"
              scene={scene}
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="TermsOfUse"
        component={TermsOfUseScreen} 
        options={{
          header: ({ navigation, scene }) => (
            <Header
              back
              title="Conditions d'utilisation"
              scene={scene}
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="LegalNotice"
        component={LegalNoticeScreen} 
        options={{
          header: ({ navigation, scene }) => (
            <Header
              back
              title="Mentions légales"
              scene={scene}
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="About"
        component={AboutScreen} 
        options={{
          header: ({ navigation, scene }) => (
            <Header
              back
              title="À propos"
              scene={scene}
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="ContactUs"
        component={ContactUsScreen} 
        options={{
          header: ({ navigation, scene }) => (
            <Header
              back
              title="Contactez-nous"
              scene={scene}
              navigation={navigation}
            />
          )
        }}
      />
      <Stack.Screen
        name="UpdateProfile"
        component={UpdateProfileScreen} 
        options={{
          header: ({ navigation, scene }) => (
            <Header
              back
              title="Modifier mon profil"
              scene={scene}
              navigation={navigation}
            />
          )
        }}
      />
      
    </Stack.Navigator>
  );
}

function getTabBarVisible(route) {
  const routeName = getFocusedRouteNameFromRoute(route) || 'Settings';

  const noTabBar = 
    [
      "Privacy", "About", "MyCourses",  "AvailableCoursesList", "CoursesList", "Lesson", "LearnExpressionCourse", "ChooseRightAnswerCourse", 
      "TranslateWriteExpressionWithBlockCourse", "PairingExpressionCourse", "FillExpressionWithBlockCourse", "TranslateExpressionByWritingCourse", "FillExpressionByWritingCourse",
      "DialogCourse", "Onboarding", "Sign In", "Sign Up", "UpdateProfile", "ContactUs", "LegalNotice", "TermsOfUse", "Subscriptions"
    ]
    .includes(routeName);

  if (noTabBar) {
    return false;
  }

  return true;
}

function AppStack(props) {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: materialTheme.COLORS.PRIMARY,
      }}
    >
      <Tab.Screen name="Home" component={HomeStack} 
        options={({ route }) => ({
          tabBarVisible: getTabBarVisible(route),
          tabBarLabel: 'Apprendre',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="book-open-variant" color={color} size={size} />
          ),
        })}
      />
      <Tab.Screen name="Profile" component={ProfileScreen} 
        options={({ route }) => ({
          tabBarLabel: 'Profil',
          tabBarVisible: getTabBarVisible(route),
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account" color={color} size={size} />
          ) 
        })}
      />
      <Tab.Screen name="Settings" component={SettingsStack} 
        options={({ route }) => ({
          tabBarLabel: 'Paramètres',
          tabBarVisible: getTabBarVisible(route),
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="cog-outline" color={color} size={size} />
          )
        })}
      />
    </Tab.Navigator>
  );
}

export default function RootNavigator(props) {
  const context = React.useContext(Context);

  return (
    <Stack.Navigator screenOptions={{ title: '', headerStyle: { height: height / 20  } }}>
      {context.token !== null ? (
        <Stack.Screen name="App" component={AppStack} />
      ) : (
        <Stack.Screen name="Onboarding" component={OnboardingStack} />
      )}
    </Stack.Navigator>
  );
}
