import React from 'react';
import { StyleSheet, ScrollView } from 'react-native';
import { Block, Text } from 'galio-framework';
import IllustrationFullQuiz from './Quiz/IllustrationFullQuiz';
import ProceedButton from './ProceedButton';
import { materialTheme } from '../constants/';

export default class LearnExpression extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { question, navigation } = this.props;

    const { expression, instruction } = question.content;

    return (
      <Block flex style={styles.base}>
        <ScrollView
          overScrollMode='always'
          showsVerticalScrollIndicator={false}
        >
          <Block>
            <Text center h5 color={materialTheme.COLORS.SECONDARY} bold>{instruction}</Text>
            <IllustrationFullQuiz 
              text={expression.text ? expression.text : undefined} 
              audioSource={expression.audio && expression.audio.url ? expression.audio.url : undefined} 
              imageUri={expression.image && expression.image.url ? expression.image.url : undefined} 
            /> 
          </Block>
        </ScrollView>
        <Block center>
          <ProceedButton navigation={navigation} />
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  base: {
    marginTop: 15,
    justifyContent: 'space-between',
  },
  button: {
    marginBottom: Platform.OS === 'ios' ? 20 : 5
  },
});