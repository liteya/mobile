import React from 'react';
import { StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { Block, theme } from 'galio-framework';
import { materialTheme } from '../constants';
import { Audio } from 'expo-av';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const { width } = Dimensions.get('screen');

export default class IconAudioPlayer extends React.Component {
  constructor(props) {
    super(props);

    this.playbackInstance = null;
    this.audioSource = null;

    this.state = {
      isPlaying: false,
      shouldPlay: false,
      volume: 1.0,
      iconName: 'volume-high'
    };
  }

  async componentDidMount() {
    try {
      await Audio.setAudioModeAsync({
        allowsRecordingIOS: false,
        interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
        playsInSilentModeIOS: true,
        interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
        shouldDuckAndroid: true,
        staysActiveInBackground: false,
        playThroughEarpieceAndroid: false
      });
 
      this.loadAudio();
    } catch (e) {
      console.log(e);
    }
  }

  async componentWillUnmount() {
    if (this.playbackInstance != null) {
      await this.playbackInstance.pauseAsync();
      await this.playbackInstance.unloadAsync();
      this.playbackInstance.setOnPlaybackStatusUpdate(null);
      this.playbackInstance = null;
    }
  }

  async loadAudio() {
    const { isPlaying, volume } = this.state;
   
    const { audioSource } = this.props;

    try {
      this.playbackInstance = new Audio.Sound();
         
      const status = {
        shouldPlay: isPlaying,
        volume
      };
      
      await this.playbackInstance.loadAsync({ uri: audioSource }, status, false);
      } catch (e) {
        console.log(e);
      }
  }

  handlePlay = async () => {
    const { isPlaying } = this.state;
    if (isPlaying) {
      await this.playbackInstance.replayAsync();

      return;
    }
      
    await this.playbackInstance.playAsync();

    this.setState({
      isPlaying: !isPlaying
    });
  }

  render() {
    return (
      <TouchableOpacity onPress={this.handlePlay}>
        <MaterialCommunityIcons name={this.state.iconName} size={40} color={materialTheme.COLORS.PRIMARY} />
      </TouchableOpacity>
    );
  }
}
