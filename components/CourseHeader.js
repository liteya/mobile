import React from 'react';
import { withNavigation } from '@react-navigation/compat';
import { TouchableOpacity, StyleSheet, Platform, Dimensions, Alert } from 'react-native';
import { Button, Block, NavBar, Input, Text, theme } from 'galio-framework';
import Context from '../store/context';
import Icon from './Icon';
import materialTheme from '../constants/Theme';

const { height, width } = Dimensions.get('window');
const iPhoneX = () => Platform.OS === 'ios' && (height === 812 || width === 812 || height === 896 || width === 896);

class CourseHeader extends React.Component {

  toggleAlertModal = (onPressExit) =>
    Alert.alert(
      "Es-tu sûr de cela ?",
      "Ta progression dans cette leçon sera perdue",
      [
        {
          text: "Annuler",
          style: "cancel"
        },
        { 
          text: "Quitter", 
          onPress: () => onPressExit()
        }
      ],
      { cancelable: false }
    );

  renderLeft = (chapter) => {
    const { navigation } = this.props;
    const onPressExit = () => navigation.navigate('CoursesList', { chapterTitle: chapter.title, chapterId : chapter.id});

    return <TouchableOpacity onPress={() => this.toggleAlertModal(onPressExit)}>
      <Icon
        size={25}
        family="font-awesome"
        name="times"
        color={ materialTheme.COLORS.SWITCH_OFF }
      />
    </TouchableOpacity>
  };

  render() {
    return (
      <Context.Consumer>
        {context => {
          return <Block style={styles.shadow}>
            <NavBar
              left={this.renderLeft(context.getChapter())}
              leftStyle={{ paddingTop: 3, flex: 0.6 }}
              leftIconColor={ materialTheme.COLORS.PRIMARY }
            />
          </Block>
        }}
      </Context.Consumer>
    );
  }
}

export default withNavigation(CourseHeader);

const styles = StyleSheet.create({
  shadow: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.2,
    elevation: 3,
  },
  content: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  contentTitle: {
    fontSize: 20,
    marginBottom: 12,
  },
});