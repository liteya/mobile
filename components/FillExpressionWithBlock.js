import React from 'react';
import { StyleSheet, TouchableWithoutFeedback, ScrollView } from 'react-native';
import { Block, Text } from 'galio-framework';
import materialTheme from '../constants/Theme';
import AlertQuiz from './Quiz/AlertQuiz';
import CheckAnswerButtonQuiz from './Quiz/CheckAnswerButtonQuiz';
import Context from '../store/context';

export default class FillExpressionWithBlock extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedBlockIds: [],
      answered: false,
      checkButtonDisabled: true
    };
  }

  async componentDidMount() {
    const selectedBlockIds = this.props.question.content.expression.blocks
      .filter(block => block.isBlank)
      .map(block => ({ "id": block.order, "fillBlockId": null })  );

    this.setState({
      selectedBlockIds: selectedBlockIds
    });
  }

  updateCheckButton() {
    this.setState(state => {
      const selectedBlockIdsNotFilled = state.selectedBlockIds.filter(item => item.fillBlockId == null);

      return {
        checkButtonDisabled: selectedBlockIdsNotFilled.length !== 0
      };
    });
  }

  isAnswerCorrect = () => {
    const { solution, blocks } = this.props.question.content;
 
    const answerCorrect = solution.map(eltSol => blocks.find(eltBlock => eltBlock.order === eltSol.id).text).join("");
    const answerInput = this.state.selectedBlockIds.map(selectedBlock => blocks.find(eltBlock => eltBlock.order === selectedBlock.fillBlockId).text).join("");

    return answerInput === answerCorrect;
  };

  checkAnswer = (addWrongAnswerQuestionList) => {
    const isAnswerCorrect = this.isAnswerCorrect();

    if (!isAnswerCorrect) {
      addWrongAnswerQuestionList();
    }

    this.setState({
      answerCorrect: isAnswerCorrect,
      answered: true 
    });
  };

  onPressUnselectBlock = (blockId) => {
    this.setState(state => {
    
      const indexToRemove = state.selectedBlockIds.findIndex(item => item.fillBlockId === blockId);
      if (indexToRemove < 0) {
        return;
      }

      const block = state.selectedBlockIds[indexToRemove];
      block.fillBlockId = null;

      state.selectedBlockIds.splice(indexToRemove, 1, block);

      return {
        selectedBlockIds: state.selectedBlockIds,
      }
    });
  };

  onPressBlock = (blockId) => {
    this.setState(state => {
      
      const nextIndexToFill = state.selectedBlockIds.findIndex(item => item.fillBlockId === null);
      if (nextIndexToFill < 0) {
        return;
      }

      const block = state.selectedBlockIds[nextIndexToFill];
      block.fillBlockId = blockId;

      state.selectedBlockIds.splice(nextIndexToFill, 1, block);

      return {
        selectedBlockIds: state.selectedBlockIds,
      };
    });
  };

  renderBlocks = (blocks) => (
    <Block flex row style={styles.blockContainer}>
      {blocks.map(block => {

        const isSelected = this.state.selectedBlockIds
          .filter(block => block.fillBlockId)
          .map(block => block.fillBlockId)
          .includes(block.order);
          
        return (
          <TouchableWithoutFeedback
            key={block.order}
            onPress={() => { if (!isSelected) this.onPressBlock(block.order); this.updateCheckButton();} }
          ><Block center style={[styles.baseBlock, isSelected ? styles.selectedBlock : styles.block]}>
              <Text style={[isSelected ? styles.selectedBlockText : styles.unselectedBlockTextNotSelected]}>
                {block.text}
              </Text>
            </Block>
          </TouchableWithoutFeedback>
        );
      })}
    </Block>
  );

  onPressIncompleteSentenceBlock = (blockId) => {
    this.setState(state => {
      return {
        selectedBlockIds: state.selectedBlockIds.concat(blockId),
      };
    });
  };

  renderIncompleteSentenceBlocks = (sentenceBlocks, blocks) => (
    <Block flex row style={styles.incompleteSentenceBlockContainer}>
      {sentenceBlocks.map(sentenceBlock => {
          if (sentenceBlock.isBlank) {
            const indexFilled = this.state.selectedBlockIds
              .filter(item => item.fillBlockId != null)
              .findIndex(item => item.id === sentenceBlock.order);

            if (indexFilled >= 0) {

              const item = this.state.selectedBlockIds
                .filter(item => item.fillBlockId != null)[indexFilled];
                
              const block = blocks.find(block => block.order === item.fillBlockId);

              return (
                <Block key={sentenceBlock.order} style={styles.incompleteSentenceBaseBlock}>
                  <TouchableWithoutFeedback
                    onPress={() => {this.onPressUnselectBlock(block.order); this.updateCheckButton();} }
                    key={sentenceBlock.order}
                  ><Block center style={[styles.block, styles.selectedSentenceBlock]}>
                      <Text style={[styles.selectedSentenceBlockText]}>
                        {block.text}
                      </Text>
                    </Block>
                  </TouchableWithoutFeedback>
                </Block>
              );
            }

            return (
              <Block key={sentenceBlock.order} style={styles.incompleteSentenceBaseBlock}>
                <Block key={sentenceBlock.order} center style={[styles.baseIncompleteSentenceBlock]}>
                  <Text>
                    {"     "}
                  </Text>
                </Block>
              </Block>
            );
          }

          return (
            <Block key={sentenceBlock.order} style={styles.incompleteSentenceBaseBlock}>
              <Text style={styles.incompleteSentenceBaseBlockText}>
                {sentenceBlock.text}
              </Text>
            </Block>
          );
      })}
    </Block>
  );

  render() {
    const { question, navigation } = this.props;

    const { instruction, blocks, expression } = question.content;

    return (
      <Context.Consumer>
        {context => {

          return (
          <Block flex center style={styles.base}>

            <ScrollView
              overScrollMode='always'
              showsVerticalScrollIndicator={false}
            >
              <Block center>
                <Text h5 color={materialTheme.COLORS.SECONDARY} bold>{instruction}</Text>
                <Text style={styles.translatedSentence}>{expression.translatedText}</Text>
                {this.renderIncompleteSentenceBlocks(expression.blocks, blocks)}
                {this.renderBlocks(blocks)}
              </Block>

            </ScrollView>

            <CheckAnswerButtonQuiz answered={this.state.answered} checkButtonDisabled={this.state.checkButtonDisabled} onPress={() => this.checkAnswer(context.addWrongAnswerQuestionList) }/>
            <AlertQuiz
              correct={this.state.answerCorrect}
              visible={this.state.answered}
              correctAnswer={expression.originalText}
              navigation={navigation}
            />
        </Block>);
        }}
      </Context.Consumer>
        
    );
  }
}

const styles = StyleSheet.create({
  base: {
    marginTop: 15,
  },
  blockContainer: {
    flexWrap: "wrap",
    justifyContent: "center",
    alignContent: 'flex-end',
    marginTop: 50
  },
  incompleteSentenceBlockContainer: {
    marginTop: 40,
    marginHorizontal: 2,
  },
  translatedSentence: {
    fontSize: 18,
    marginTop: 20,
    marginHorizontal: 2,
    color: materialTheme.COLORS.QUESTION_QUIZ_COLOR_GRAY
  },
  baseBlock: {
    padding: 10,
    margin: 8,
    borderRadius: 10,
    borderWidth: 1,
  },
  block: {
    backgroundColor: "white",
    borderColor: "white"
  },
  selectedBlock: {
    backgroundColor: materialTheme.COLORS.DEFAULT,
    borderColor: materialTheme.COLORS.DEFAULT
  },
  selectedBlockText: {
    color: materialTheme.COLORS.DEFAULT
  },
  unselectedBlockTextNotSelected: {
    color: "black"
  },
  incompleteSentenceBaseBlock: {
    padding: 3,
    paddingTop: 8,
  },
  incompleteSentenceBaseBlockText: {
    fontSize: 20
  },
  baseIncompleteSentenceBlock: {
    borderBottomWidth: 1,
    borderBottomColor: materialTheme.COLORS.QUESTION_QUIZ_COLOR_GRAY,
  },
  selectedSentenceBlock: {
    borderRadius: 10,
    borderWidth: 1,
    marginTop: -8, 
  },
  selectedSentenceBlockText: {
    fontSize: 20,
    padding: 8
  }
});
