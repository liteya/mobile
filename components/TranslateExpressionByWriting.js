import React from 'react';
import { StyleSheet, TextInput, Keyboard } from 'react-native';
import { Block, Text } from 'galio-framework';
import materialTheme from '../constants/Theme';
import AlertQuiz from './Quiz/AlertQuiz';
import CheckAnswerButtonQuiz from './Quiz/CheckAnswerButtonQuiz';
import IllustrationFullQuiz from './Quiz/IllustrationFullQuiz';
import Context from '../store/context';

export default class TranslateExpressionByWriting extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      inputUserAnswer: null,
      answered: false,
      checkButtonDisabled: true
    };
  }

  onChangeText = (text) => {
    this.setState({
      inputUserAnswer: text,
      checkButtonDisabled: text == ""
    });
  }

  updateCheckButton() {
    this.setState(state => {
      return {
        checkButtonDisabled: state.selectedBlockIds.length == 0
      };
    });
  }

  renderTextInput() {
    return (
      <TextInput
        placeholder="Veuillez saisir ici"
        autoCapitalize="none"
        multiline
        numberOfLines={2}
        style={styles.input}
        textAlignVertical="top"
        placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
        onChangeText={text => this.onChangeText(text)}
      />
    );
  }

  checkAnswer = (addWrongAnswerQuestionList) => {
    const isAnswerCorrect = this.state.inputUserAnswer.toLowerCase().replace(/\s+/g, '') === this.props.question.content.solution.toLowerCase().replace(/\s+/g, '');

    if (!isAnswerCorrect) {
      addWrongAnswerQuestionList();
    }
    
    Keyboard.dismiss();

    this.setState({
      answerCorrect: isAnswerCorrect,
      answered: true 
    });
  };

  render() {
    const { question, navigation } = this.props;

    const { solution, expression, instruction } = question.content;
 
    return (
      <Context.Consumer>
        {context => {
          return (<Block flex center style={styles.blockSentence}>
          <Block>
            <Text h5 center color={materialTheme.COLORS.SECONDARY} bold>{instruction}</Text>
            {expression && <IllustrationFullQuiz 
              text={expression.text ? expression.text : undefined} 
              audioSource={expression.audio && expression.audio.url ? expression.audio.url : undefined} 
              imageUri={expression.image && expression.image.url ? expression.image.url : undefined} /> }
            {this.renderTextInput()}
          </Block>
          <CheckAnswerButtonQuiz answered={this.state.answered} checkButtonDisabled={this.state.checkButtonDisabled} onPress={() => this.checkAnswer(context.addWrongAnswerQuestionList) }/>
          <AlertQuiz
            correct={this.state.answerCorrect}
            visible={this.state.answered}
            correctAnswer={solution}
            navigation={navigation}
          />
        </Block>);
        }}
      </Context.Consumer>    
    );
  }
}

const styles = StyleSheet.create({
  blockSentence: {
    justifyContent: 'space-between',
    marginTop: 15,
  },
  sentence: {
    color: materialTheme.COLORS.CAPTION,
  },
  baseBlock: {
    padding: 8,
    margin: 6,
    borderRadius: 10,
    borderWidth: 1,
  },
  input: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: materialTheme.COLORS.PLACEHOLDER,
    backgroundColor: "white",
    padding: 10,
  },
});
