import React from 'react';
import { StyleSheet, Dimensions, TextInput, Keyboard } from 'react-native';
import { Block, Text, theme } from 'galio-framework';
import materialTheme from '../constants/Theme';
import AlertQuiz from './Quiz/AlertQuiz';
import CheckAnswerButtonQuiz from './Quiz/CheckAnswerButtonQuiz';
import Context from '../store/context';

const { width } = Dimensions.get('screen');
export default class FillExpressionByWriting extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      inputUserAnswer: null,
      answered: false,
      checkButtonDisabled: true
    };
  }

  onChangeText = (text) => {
    this.setState({
      inputUserAnswer: text,
      checkButtonDisabled: text == ""
    });
  }

  renderIncompleteSentenceBlocks = (sentenceBlocks) => (
    <Block flex center row style={styles.incompleteSentenceBlockContainer}>
      {sentenceBlocks.map(sentenceBlock => {
        
          if (sentenceBlock.isBlank) {
            return (
              <Block key={sentenceBlock.order} style={styles.incompleteSentenceBaseBlock}>
                <Block key={sentenceBlock.order} center style={[styles.baseIncompleteSentenceBlock]}>
                  <Text>
                    {"     "}
                  </Text>
                </Block>
              </Block>
            );
          }

          return (
            <Block key={sentenceBlock.order} style={styles.incompleteSentenceBaseBlock}>
              <Text>
                {sentenceBlock.text}
              </Text>
            </Block>
          );
      })}
    </Block>
  );

  updateCheckButton() {
    this.setState(state => {
      return {
        checkButtonDisabled: state.selectedBlockIds.length == 0
      };
    });
  }

  renderTextInput() {
    return (
      <TextInput
        placeholder="Veuillez saisir ici"
        autoCapitalize="none"
        multiline
        numberOfLines={2}
        style={styles.input}
        textAlignVertical="top"
        placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
        onChangeText={text => this.onChangeText(text)}
      />
    );
  }

  checkAnswer = (addWrongAnswerQuestionList) => {
    const isAnswerCorrect = this.state.inputUserAnswer.toLowerCase().replace(/\s+/g, '') === this.props.question.content.solution.toLowerCase().replace(/\s+/g, '');

    if (!isAnswerCorrect) {
      addWrongAnswerQuestionList();
    }

    Keyboard.dismiss();

    this.setState({
      answerCorrect: isAnswerCorrect,
      answered: true 
    });
  };

  nextQuestion = () => {
    return this.props.navigation.pop();
  };

  render() {
    const { question, navigation } = this.props;

    const { instruction, solution, expression } = question.content;
 
    return (
      <Context.Consumer>
        {context => {
          return (<Block flex center style={styles.base}>
          <Block>
            <Text h5 center color={materialTheme.COLORS.SECONDARY} bold>{instruction}</Text>
            {this.renderIncompleteSentenceBlocks(expression.blocks)}
            {this.renderTextInput()}
            <CheckAnswerButtonQuiz answered={this.state.answered} checkButtonDisabled={this.state.checkButtonDisabled} onPress={() => this.checkAnswer(context.addWrongAnswerQuestionList) }/>
          </Block>
          <AlertQuiz
            correct={this.state.answerCorrect}
            visible={this.state.answered}
            correctAnswer={solution}
            navigation={navigation}
          />
        </Block>);
        }}
      </Context.Consumer>
    );
  }
}

const styles = StyleSheet.create({
  base: {
    marginTop: 15,
    width: width,    
    marginTop: theme.SIZES.BASE * 0.5, 
    paddingHorizontal: theme.SIZES.BASE, 
  },
  input: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: materialTheme.COLORS.PLACEHOLDER,
    backgroundColor: "white",
    padding: 10,
    marginBottom: 20
  },
  incompleteSentenceBlockContainer: {
    flexWrap: "wrap",
    marginTop: 10,
  },
  incompleteSentenceBaseBlock: {
    padding: 3,
    paddingTop: 8,
  },
  baseIncompleteSentenceBlock: {
    borderBottomWidth: 1,
    borderBottomColor: materialTheme.COLORS.QUESTION_QUIZ_COLOR_GRAY,
  },
});
