import React from 'react';
import { StyleSheet, Dimensions, TouchableWithoutFeedback } from 'react-native';
import { Block, Text, theme } from 'galio-framework';
import materialTheme from '../constants/Theme';
import AlertQuiz from './Quiz/AlertQuiz';
import CheckAnswerButtonQuiz from './Quiz/CheckAnswerButtonQuiz';

const { width } = Dimensions.get('screen');

export default class PairingExpression extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedToPairBlockIds: [],
      validatedPairsBlockIds: [],
      validPairingBlockIds: [],
      invalidPairingBlockIds: [],
      answered: false,
      checkButtonDisabled: true
    };
  }

  validAnswer = () => {
    this.setState({
      answered: true,
      answerCorrect: true
    });
  }

  isAnsweredPairCorrect = (answeredPairBlocks) => {

    const { solution } = this.props.question.content;
    for (let i = 0; i < solution.length; i++) {
      if (answeredPairBlocks.includes(solution[i]['first_pair_id']) && answeredPairBlocks.includes(solution[i]['second_pair_id'])) {
        return true;
      }
    }
  
    return false;
  };

  onPressBlock = (blockId) => {
    const { selectedToPairBlockIds, validatedPairsBlockIds } = this.state;

    const isValidatedToPair = validatedPairsBlockIds.includes(blockId);
    if (isValidatedToPair) {
      return;
    }

    const isSelectedToPair = selectedToPairBlockIds.includes(blockId);

    if (selectedToPairBlockIds.length === 1) {
      const answeredPairBlocks = selectedToPairBlockIds.concat(blockId);
      const isAnsweredPairCorrect = this.isAnsweredPairCorrect(answeredPairBlocks)

      if (isAnsweredPairCorrect) {
        this.setState(state => {
          return {
            selectedToPairBlockIds: [],
            validPairingBlockIds: state.validatedPairsBlockIds.concat(answeredPairBlocks)
          };
        },
        () => {
          setTimeout(() => {
            this.setState(state => {
              return {
                selectedToPairBlockIds: [],
                validPairingBlockIds: [],
                validatedPairsBlockIds: state.validatedPairsBlockIds.concat(answeredPairBlocks)
              };
            },
            () => {
              const { blocks } = this.props.question.content;

              if (this.state.validatedPairsBlockIds.length === blocks.length) {
                this.validAnswer();
              }
            }, 300);
          }, 500);
        });

        return;
      }

      this.setState(state => {
        return {
          selectedToPairBlockIds: [],
          invalidPairingBlockIds: state.validatedPairsBlockIds.concat(answeredPairBlocks)
        };
      },
      () => {
        setTimeout(() => {
          this.setState(state => {
            return {
              selectedToPairBlockIds: [],
              invalidPairingBlockIds: [],
            };
          });
        }, 750);
      });

      return;
    }

    if (!isSelectedToPair) { 
      this.setState(state => {
        return {
          selectedToPairBlockIds: selectedToPairBlockIds.concat(blockId),
        };
      });

      return;
    }
  }

  renderBlocks = (blocks) => (
    <Block flex row style={styles.blockContainer}>
      {blocks.map(block => {
        const isSelectedToPair = this.state.selectedToPairBlockIds.includes(block.id);
        const isValidatedToPair = this.state.validatedPairsBlockIds.includes(block.id);
        const validPairing = this.state.validPairingBlockIds.includes(block.id);
        const invalidPairing = this.state.invalidPairingBlockIds.includes(block.id);

        let blockStyle = styles.block;
        let textStyle = styles.blockText; 

        if (isSelectedToPair) {
          blockStyle = styles.selectedToPairBlock;
        } else if (isValidatedToPair) {
          blockStyle = styles.validatedToPairBlock;
          textStyle = styles.validatedBlockText; 
        } else if (validPairing) {
          blockStyle = styles.validPairingBlock;
          textStyle = styles.validPairingBlockText; 
        } else if (invalidPairing) {
          blockStyle = styles.invalidPairingBlock;
          textStyle = styles.invalidPairingBlockText; 
        }

        return (
          <TouchableWithoutFeedback
            key={block.id}
            onPress={() => this.onPressBlock(block.id)}
          ><Block center style={[styles.baseBlock, blockStyle]}>
              <Text style={textStyle}>
                {block.text}
              </Text>
            </Block>
          </TouchableWithoutFeedback>
        );
      })}
    </Block>
  );

  render() {
    const { question, navigation } = this.props;

    const { blocks, instruction } = question.content;

    return (
      <Block flex center style={styles.base}>
        <Text h5 center color={materialTheme.COLORS.SECONDARY} bold>{instruction}</Text>
        {this.renderBlocks(blocks)}
        <CheckAnswerButtonQuiz answered={this.state.answered} checkButtonDisabled={this.state.checkButtonDisabled} onPress={() => this.checkAnswer() }/>
        <AlertQuiz
          correct={this.state.answerCorrect}
          visible={this.state.answered}
          navigation={navigation}
        />
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  base: {
    marginTop: 15,
  },
  blockContainer: {
    flexWrap: "wrap",
    alignContent: 'center',
    justifyContent: 'center',
  },
  baseBlock: {
    padding: 8,
    margin: 6,
    borderRadius: 10,
    borderWidth: 1
  },
  block: {
    backgroundColor: "white",
    borderColor: "white"
  },
  selectedToPairBlock: {
    borderColor: materialTheme.COLORS.BUTTON_QUIZ_SELECTED_BORDER_COLOR,
    backgroundColor: materialTheme.COLORS.BUTTON_QUIZ_SELECTED_BACKGROUND_COLOR,
  },
  validatedToPairBlock: {
    borderColor: materialTheme.COLORS.DEFAULT,
    backgroundColor: materialTheme.COLORS.BLOCK_VALIDATED_BACKGROUND_COLOR,
  },
  blockText: {
    color: "black"
  },
  validatedBlockText: {
    color: materialTheme.COLORS.BLOCK_TEXT_VALIDATED_BACKGROUND_COLOR
  },
  validPairingBlock: {
    borderColor: materialTheme.COLORS.SUCCESS,
    backgroundColor: materialTheme.COLORS.BLOCK_VALID_BACKGROUND_COLOR,
  },
  validPairingBlockText: {
    color: materialTheme.COLORS.SUCCESS
  },
  invalidPairingBlock: {
    borderColor: materialTheme.COLORS.ERROR,
    backgroundColor: materialTheme.COLORS.BLOCK_INVALID_BACKGROUND_COLOR,
  },
  invalidPairingBlockText: {
    color: materialTheme.COLORS.ERROR
  },
});
