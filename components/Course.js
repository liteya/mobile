import React from 'react';
import { withNavigation } from '@react-navigation/compat';
import { StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { Block, Text, theme } from 'galio-framework';
import Icon from './Icon';
import Context from '../store/context';
import { materialTheme } from '../constants';

const COURSE_COMPLETED = 'COMPLETED';

class Course extends React.Component {

  onPress(setCourse, course, setCurrentQuestionOrder) {
    const { navigation } = this.props;
    
    course.questions.sort((a, b) => a.order - b.order);

    setCourse(course);

    const currentQuestionOrder = 0;
    
    const firstQuestion = course.questions[currentQuestionOrder];
    setCurrentQuestionOrder(currentQuestionOrder + 1);

    navigation.navigate(`${firstQuestion.id.type}Course`, { currentQuestion: firstQuestion})
  }
  
  renderIcon() {
    const { status } = this.props;

    let iconName = status === COURSE_COMPLETED ? 'check' : 'angle-double-down';
    let iconColor = status === COURSE_COMPLETED ? materialTheme.COLORS.SECONDARY : materialTheme.COLORS.PRIMARY;

    return (
      <Block flex center style={styles.courseIconStatus}>
        <Icon size={25} color={iconColor} family="font-awesome" name={iconName} />
      </Block>
    );
  }

  render() {
    const { courseId, status } = this.props;

    return (
      <Context.Consumer>
        {context => {
          const chapter = context.getChapter();

          const course = chapter.courses.find(course => course.id.id === courseId).id;

          const borderColor = status === COURSE_COMPLETED ? theme.COLORS.WHITE : materialTheme.COLORS.PRIMARY;

          return (
            <Block card flex style={[styles.course, styles.shadow, { 'borderColor' : borderColor }]}>
              {this.renderIcon()}
              <TouchableWithoutFeedback onPress={() => this.onPress(context.setCourse, course, context.setCurrentQuestionOrder)}>
                <Block flex center space="between" style={styles.courseInformation}>
                  <Text size={18} style={styles.courseTitle}>{course.title}</Text>
                  <Text size={12} muted style={styles.courseTitle}>{course.description}</Text>
                </Block>
              </TouchableWithoutFeedback> 
            </Block>
          )
        }}
      </Context.Consumer>
    );
  }
}

export default withNavigation(Course);

const styles = StyleSheet.create({
  course: {
    backgroundColor: theme.COLORS.WHITE,
    marginVertical: theme.SIZES.BASE,
    borderWidth: 1,
    minHeight: 114,
  },
  courseTitle: {
    textAlign: 'center',
    paddingBottom: 6,
  },
  courseIconStatus: {
    marginTop: 5
  },
  courseInformation: {
    padding: theme.SIZES.BASE / 2,
    marginBottom: 20,
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 3,
    shadowOpacity: 0.1,
    elevation: 2,
  },
});