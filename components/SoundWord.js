import React from 'react';
import { StyleSheet, TouchableHighlight } from 'react-native';
import { Text, theme, Block } from 'galio-framework';
import { Audio } from 'expo-av';

import materialTheme from '../constants/Theme';

export default class SoundWord extends React.Component {

  render() {
    const { audioSource, dotted, children } = this.props;

    const borderStyle = dotted ? styles.dotted : styles.normal;

    const onPress = async () => {
      const soundObject = new Audio.Sound();
      try {
        await soundObject.loadAsync(audioSource);
        await soundObject.playAsync();

      } catch (error) {
      }
    };

    return (
      <TouchableHighlight style={{ 'marginHorizontal': theme.SIZES.BASE * 0.2 }} underlayColor={ materialTheme.COLORS.WARNING }
          onPress={onPress}
        >
          <Block center style={[styles.base, styles.shadow, borderStyle]}>
            <Text>{children}</Text>
          </Block>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  base: {
    paddingHorizontal: 6,
    paddingVertical: 10,
    borderRadius: 5,
    borderWidth: 1,
    minWidth: 30,
  },
  dotted: {
    borderStyle: 'dotted',
    borderColor: materialTheme.COLORS.PRIMARY,
  },
  normal: {
    borderColor: theme.COLORS.WHITE,
    backgroundColor: theme.COLORS.WHITE,
  },
  shadow: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: { width: 0, height: 7 },
    shadowRadius: 20,
    shadowOpacity: 1
  },
});