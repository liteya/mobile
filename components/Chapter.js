import React from 'react';
import { withNavigation } from '@react-navigation/compat';
import { StyleSheet, Image, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { Block, Text, theme } from 'galio-framework';
import Icon from './Icon';
import Context from '../store/context';
import { materialTheme } from '../constants';
import { Alert } from 'react-native';
const { width, height } = Dimensions.get('screen');

const CHAPTER_COMPLETED = 'COMPLETED';
const CHAPTER_LOCKED = 'LOCKED';
class Chapter extends React.Component {

  onPress(setChapter, user) {
    const { navigation, chapter, status } = this.props;
    const premiumAccessActivated = user && user.premiumAccess ? user.premiumAccess.activated : false;

    if (status !== CHAPTER_COMPLETED && !premiumAccessActivated && chapter.premiumAccess) {
      return navigation.navigate('Subscriptions');
    }

    if (status === CHAPTER_LOCKED) {
      return Alert.alert(
        "Tu y es presque !",
        "Tu dois terminer toutes les leçons du chapitre précédent pour débloquer le suivant"
      );
    }

    setChapter(chapter);

    return navigation.navigate('CoursesList', { chapterTitle: chapter.title, chapterId : chapter.id, chapterCourses: chapter.courses});
  }

  renderCounter() {
    const { chapter, status } = this.props;
    let counterColor = materialTheme.COLORS.PRIMARY; 
    if (status === CHAPTER_COMPLETED) {
      counterColor = materialTheme.COLORS.SECONDARY;
    } else if (status === CHAPTER_LOCKED) {
      counterColor = materialTheme.COLORS.MUTED;
    }

    return (chapter.courses.length !== 0 ?
      <Text size={13} style={[{ 'color' : counterColor }]}>{chapter.courses.length} {'leçons'}</Text>
      :
      <Text></Text>
    );
  }

  render() {
    const { chapter, status } = this.props;

    let borderColor = materialTheme.COLORS.PRIMARY;
    let iconName = 'angle-double-down';
    let iconColor = materialTheme.COLORS.PRIMARY; 
    let counterColor = materialTheme.COLORS.MUTED; 
    let isTitleMuted = false;
    if (status === CHAPTER_LOCKED) {
      iconName = 'lock';
      iconColor = materialTheme.COLORS.MUTED;
      borderColor = theme.COLORS.WHITE;
      isTitleMuted = true;
    } else if (status === CHAPTER_COMPLETED) {
      iconName = 'check';
      counterColor = materialTheme.COLORS.PRIMARY;
      borderColor = theme.COLORS.WHITE;
      iconColor = materialTheme.COLORS.SECONDARY; 
    }
    return (
      <Context.Consumer>
        {context => {
          const user = context.getCurrentUser();
          const premiumAccessActivated = user && user.premiumAccess ? user.premiumAccess.activated : false;

          return (
            <TouchableWithoutFeedback onPress={() => this.onPress(context.setChapter, user)}>
              <Block card flex style={[styles.chapter, styles.shadow, { 'borderColor' : borderColor }]}>
                <Block flex center style={styles.chapterIconStatus}>
                  {status !== CHAPTER_COMPLETED && !premiumAccessActivated && chapter.premiumAccess && <Icon size={24} color={materialTheme.COLORS.WARNING} family="font-awesome" name="graduation-cap" />}
                  <Icon size={23} color={iconColor} family="font-awesome" name={iconName} />
                  <Text size={15} muted={isTitleMuted} style={styles.chapterTitle}>{chapter.title}</Text>
                </Block>
                
                <Block style={[styles.imageContainer, styles.shadow]}>

                  <Block style={styles.blockChapterTitle}>
                    <Text size={11} muted style={styles.chapterTitle}>{chapter.description}</Text>
                  </Block>

                  <Block style={styles.blockChapterImage}>
                    {chapter.image && <Image source={{ uri: chapter.image.url }} style={styles.image} />}
                  </Block>

                </Block>

                <Block flex center style={styles.chapterInformation}>
                  {this.renderCounter(chapter)}
                </Block>
                
              </Block>
            </TouchableWithoutFeedback>
          )
        }}
      </Context.Consumer>
      
    );
  }
}

export default withNavigation(Chapter);

const styles = StyleSheet.create({
  chapter: {
    backgroundColor: theme.COLORS.WHITE,
    marginVertical: theme.SIZES.BASE,
    borderWidth: 1,
    minHeight: 114,
  },
  blockChapterTitle: {
    flex: 1,
    paddingHorizontal: 2
  },
  blockChapterImage: {
    flex: 1,
    paddingHorizontal: 2
  },
  chapterTitle: {
    textAlign: 'center',
    paddingBottom: 6,
  },
  chapterIconStatus: {
    marginTop: 5
  },
  chapterInformation: {
    padding: theme.SIZES.BASE / 2,
    marginBottom: 5,
  },
  imageContainer: {
    alignItems: 'center',
    alignSelf: 'center',
    alignContent: 'space-between',
    flexDirection: 'row',
    width: width / 1.15,
  },
  image: {
    borderRadius: 3,
    height: height / 8,
    width: width / 2.5,
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 3,
    shadowOpacity: 0.1,
    elevation: 2,
  },
});