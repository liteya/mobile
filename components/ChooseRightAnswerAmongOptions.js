import React from 'react';
import { StyleSheet, StatusBar, SafeAreaView, ScrollView } from 'react-native';
import { Block, Text } from 'galio-framework';
import { materialTheme } from '../constants';
import CheckAnswerButtonQuiz from './Quiz/CheckAnswerButtonQuiz';
import ButtonQuiz from './Quiz/ButtonQuiz';
import AlertQuiz from './Quiz/AlertQuiz';
import ButtonQuizContainer from './Quiz/ButtonQuizContainer';
import IllustrationFullQuiz from './Quiz/IllustrationFullQuiz';
import Context from '../store/context';

export default class ChooseRightAnswerAmongOptions extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      answered: false,
      answerCorrect: false,
      selectedIndex: null,
      checkButtonDisabled: true
    };
  }

  checkAnswer = () => {
    this.setState({
      answered: true 
    });
  }

  selectionOnPress = (answerIndex, answerCorrect, addWrongAnswerQuestionList) => {
    if (!answerCorrect) {
      addWrongAnswerQuestionList();
    }

    this.setState({
      selectedIndex: answerIndex,
      checkButtonDisabled: false,
      answerCorrect: answerCorrect
    });
  }

  render() {
    
    const { question, navigation } = this.props;

    const { instruction, possible_answers, expression } = question.content;
    const correctAnswer = possible_answers.find(answer => answer.isCorrect);

    return (
      <Block style={styles.base}>
        <StatusBar barStyle="light-content" />
        <ScrollView
          overScrollMode='always'
          showsVerticalScrollIndicator={false}
        >
          <Block>
            <Text h5 center color={materialTheme.COLORS.SECONDARY} bold>{instruction}</Text>
            {expression && <IllustrationFullQuiz 
              text={expression.text ? expression.text : undefined} 
              audioSource={expression.audio && expression.audio.url ? expression.audio.url : undefined} 
              imageUri={expression.image && expression.image.url ? expression.image.url : undefined} /> }

                <Context.Consumer>
                  {context => {
                    return (
                      <ButtonQuizContainer isDisabled={this.state.answered}>
                        {possible_answers.map(answer => (
                          <ButtonQuiz
                            selectedIndex={this.state.selectedIndex}
                            imageUri={answer.image ? answer.image.url : ''}
                            key={answer.order}
                            index={answer.order}
                            onSelect={() => this.selectionOnPress(answer.order, answer.isCorrect, context.addWrongAnswerQuestionList)}
                            audioSource={answer.audio ? answer.audio.url : ''}
                          >{answer.text}</ButtonQuiz>
                        ))}
                      </ButtonQuizContainer>
                    );
                  }}
                </Context.Consumer>

          </Block>
        </ScrollView>
        <CheckAnswerButtonQuiz correct={this.state.answerCorrect} answered={this.state.answered} checkButtonDisabled={this.state.checkButtonDisabled} onPress={() => this.checkAnswer() }/>
        <AlertQuiz
          correct={this.state.answerCorrect}
          visible={this.state.answered}
          correctAnswer={correctAnswer.text}
          navigation={navigation}
        />
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  base: {
    marginTop: 15,
    flex: 1,
    paddingHorizontal: 20
  },
  text: {
    color: materialTheme.COLORS.QUESTION_QUIZ_COLOR_GRAY,
    fontSize: 21,
    textAlign: "center",
    letterSpacing: -0.02,
    fontWeight: "600"
  },
  safearea: {
    flex: 1,
    justifyContent: "space-between"
  }
});
