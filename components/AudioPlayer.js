import React from 'react';
import { StyleSheet, Dimensions, Image, TouchableOpacity, Slider } from 'react-native';
import { Block, theme } from 'galio-framework';
import { materialTheme } from '../constants';
import { Audio } from 'expo-av';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const { width } = Dimensions.get('screen');

export default class AudioPlayer extends React.Component {
  constructor(props) {
    super(props);

    this.isSeeking = false;
    this.shouldPlayAtEndOfSeek = false;
    this.playbackInstance = null;
    this.imageUri = null;
    this.audioSource = null;

    this.state = {
      isPlaying: true,
      shouldPlay: false,
      playbackInstancePosition: null,
      playbackInstanceDuration: null,
      currentIndex: 0,
      volume: 1.0,
      isBuffering: false,
    };
  }

  async componentDidMount() {
    try {
      await Audio.setAudioModeAsync({
        allowsRecordingIOS: false,
        interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
        playsInSilentModeIOS: true,
        interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
        shouldDuckAndroid: true,
        staysActiveInBackground: false,
        playThroughEarpieceAndroid: false
      });
 
      this.loadAudio();
    } catch (e) {
      console.log(e);
    }
  }

  async componentWillUnmount() {
    
    if (this.playbackInstance != null) {
      await this.playbackInstance.unloadAsync();
      this.playbackInstance.setOnPlaybackStatusUpdate(null);
      this.playbackInstance = null;
    }
  }

  async loadAudio() {
    const { audioSource } = this.props;

    const { isPlaying, volume } = this.state;
   
    try {
      this.playbackInstance = new Audio.Sound();
   
      this.playbackInstance.setOnPlaybackStatusUpdate(this.onPlaybackStatusUpdate);   
      
      const status = {
        shouldPlay: isPlaying,
        volume
      };
      
      // todo : clean this
      const audioSplit = audioSource.split('audio');
      const newAudioSource = 'https://d1l8lrujynwqn3.cloudfront.net/audio' + audioSplit[1];

      //await this.playbackInstance.loadAsync({ uri: newAudioSource }, status, false);
      this.playbackInstance.loadAsync(
        { uri: newAudioSource },
        status
      ).then((res)=>{
        res.sound.setOnPlaybackStatusUpdate((status)=>{
          if(!status.didJustFinish) return;
          console.log('Unloading '+name);
          res.sound.unloadAsync().catch(()=>{});
        });
      }).catch((error)=>{});
      } catch (e) {
        console.log(e);
      }
  }
  
  onPlaybackStatusUpdate = status => {
    if (status.isLoaded) {
			this.setState({
				playbackInstancePosition: status.positionMillis,
				playbackInstanceDuration: status.durationMillis,
				shouldPlay: status.shouldPlay,
				isPlaying: status.isPlaying,
				isBuffering: status.isBuffering,
				volume: status.volume,
      });
      
			if (status.didJustFinish) {
        this.setState({
          playbackInstancePosition: null,
          playbackInstanceDuration: null,
        });

        if (this.playbackInstance != null) {
          this.playbackInstance.setOnPlaybackStatusUpdate(null);
          this.playbackInstance = null;
          this.loadAudio();
        }
			}
		} else {
			if (status.error) {
				console.log(`FATAL PLAYER ERROR: ${status.error}`);
			}
		}
  }

  handlePlayPause = async () => {
    const { isPlaying } = this.state
    isPlaying ? await this.playbackInstance.pauseAsync() : await this.playbackInstance.playAsync();
 
    this.setState({
      isPlaying: !isPlaying
    });
  }

  getSeekSliderPosition() {
    let { playbackInstancePosition, playbackInstanceDuration } = this.state;
		if (
			this.playbackInstance != null &&
			playbackInstancePosition != null &&
      playbackInstanceDuration != null && 
      !this.isSeeking
		) {
			return (
				playbackInstancePosition /
				playbackInstanceDuration
			);
		}
		return 0;
  }

  onSeekSliderSlidingComplete = value => {
		if (this.playbackInstance != null) {
      this.isSeeking = false;
      const seekPosition = value * this.state.playbackInstanceDuration;
      this.shouldPlayAtEndOfSeek = this.state.shouldPlay;
      if (this.shouldPlayAtEndOfSeek) {
				this.playbackInstance.playFromPositionAsync(seekPosition);
			} else {
				this.playbackInstance.setPositionAsync(seekPosition);
			}
		}
	};
  
  onSeekSliderValueChange = async value => {
    if (this.playbackInstance != null && !this.isSeeking) {
			this.isSeeking = true;
			this.shouldPlayAtEndOfSeek = this.state.shouldPlay;
		}
  };
  
  renderImage = () => {
    const { imageUri } = this.props;

    if (imageUri) {
      return <Image
        style={styles.albumCover}
        source={{ uri: imageUri }}
      />;
    }
  }

  render() {
    return (
      <Block style={styles.container}>
        {this.renderImage()}  
        <Block row style={styles.player}>
          <TouchableOpacity onPress={this.handlePlayPause}>
            {this.state.isPlaying ? (
              <MaterialCommunityIcons name='pause' size={35} color='#FFFFFF' />
              ) : (
              <MaterialCommunityIcons name='play' size={35} color='#FFFFFF' />
            )}
          </TouchableOpacity>
          <Block flex style={styles.playbackContainer}>
            <Slider
              style={styles.playbackSlider}
              value={this.getSeekSliderPosition()}
              onValueChange={this.onSeekSliderValueChange}
              onSlidingComplete={this.onSeekSliderSlidingComplete}
              thumbTintColor="#FFFFFF"
              minimumTrackTintColor="#FFFFFF"
              maximumTrackTintColor={materialTheme.COLORS.PLACEHOLDER}
            />
          </Block>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
  },
  albumCover: {
    width: width - theme.SIZES.BASE * 5,
    height: 140
  },
  player: {
    backgroundColor: materialTheme.COLORS.PRIMARY,
    width: width - theme.SIZES.BASE * 5,
    height: theme.SIZES.BASE * 2,
    paddingRight: 10,
  },
  playbackSlider: {
		alignSelf: 'stretch',
    marginTop: Platform.OS === 'ios' ? -3 : 10,
  },
  playbackContainer: {
    height: 80
	},
});
