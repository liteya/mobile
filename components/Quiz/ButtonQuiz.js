import React from "react";
import { TouchableOpacity, Text, StyleSheet, Image } from "react-native";
import { materialTheme } from '../../constants';
import { Audio } from 'expo-av';

export default class ButtonQuiz extends React.Component {
  constructor(props) {
    super(props);

    this.playbackInstance = null;
  }

  async componentDidMount() {
    try {
      await Audio.setAudioModeAsync({
        allowsRecordingIOS: false,
        interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
        playsInSilentModeIOS: true,
        interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
        shouldDuckAndroid: true,
        staysActiveInBackground: false,
        playThroughEarpieceAndroid: false
      });
 
    } catch (e) {
      console.log(e);
    }
  }

  async componentWillUnmount() {
    if (this.playbackInstance != null) {
      await this.playbackInstance.pauseAsync();
      await this.playbackInstance.unloadAsync();
      this.playbackInstance.setOnPlaybackStatusUpdate(null);
      this.playbackInstance = null;
    }
  }

  renderImage = () => {
    const { imageUri } = this.props;
    if (imageUri) {
      return <Image
        style={styles.illustration}
        source={{ uri: imageUri }}
      />;
    }
  }

  playAudio = async () => {
    const { audioSource  } = this.props;
    this.playbackInstance = new Audio.Sound();

    try {
      await this.playbackInstance.loadAsync({ uri: audioSource });
      await this.playbackInstance.playAsync();

    } catch (error) {
      console.log(error);
    }
  };

  onPress = () => {
    const { onSelect, audioSource } = this.props;
    audioSource && this.playAudio();
    onSelect && onSelect();
  };

  render() {
    const { children, index, selectedIndex } = this.props;

    return (
      <TouchableOpacity activeOpacity={0.7} onPress={this.onPress} style={[styles.button, selectedIndex === index ? styles.selected : '']}>
        {this.renderImage()}
        <Text style={styles.text}>{children}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: materialTheme.COLORS.BUTTON_QUIZ_COLOR,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: materialTheme.COLORS.DEFAULT,
    paddingVertical: 10,
    alignItems: "center",
    justifyContent: "center",
    width: "46%",
    marginTop: 10
  },
  selected: {
    borderColor: materialTheme.COLORS.BUTTON_QUIZ_SELECTED_BORDER_COLOR,
    backgroundColor: materialTheme.COLORS.BUTTON_QUIZ_SELECTED_BACKGROUND_COLOR,
  },
  text: {
    fontSize: 20,
    textAlign: "center"
  },
  illustration: {
    width: 110,
    height: 80,
    marginBottom: 10,
    marginHorizontal: 20,
  },
});