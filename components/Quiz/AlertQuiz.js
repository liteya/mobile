import React from "react";
import { StyleSheet, Dimensions, Platform, Alert } from "react-native";
import { Block, Text, Button } from 'galio-framework';
import { materialTheme } from "../../constants";
import Icon from '../Icon';
import { Audio } from 'expo-av';
import ProceedButton from '../ProceedButton';
import Sounds from '../../constants/Sounds';

const screen = Dimensions.get("window");
const { width } = Dimensions.get('screen');

export default class AlertQuiz extends React.Component {
  constructor(props) {
    super(props);

    this.playbackInstance = null;
  }

  state = {
    hasPlayed: false
  };

  async componentDidMount() {
    try {
      await Audio.setAudioModeAsync({
        allowsRecordingIOS: false,
        interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
        playsInSilentModeIOS: true,
        interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
        shouldDuckAndroid: true,
        staysActiveInBackground: false,
        playThroughEarpieceAndroid: false
      });
      
      this.playbackInstance = new Audio.Sound();
    } catch (e) {
      console.log(e);
    }
  }

  async componentWillUnmount() {

    if (this.playbackInstance != null) {
      await this.playbackInstance.unloadAsync();
      this.playbackInstance = null;
    }
  }

  playAnsweredBip = async () => {
    const { correct } = this.props;
    const audioSource = correct ? Sounds.QuizSuccess : Sounds.QuizError;

    try {

      if (!this.state.hasPlayed) {
        await this.playbackInstance.unloadAsync();
        await this.playbackInstance.loadAsync({ uri: audioSource });
        await this.playbackInstance.playAsync();
        this.setState({ hasPlayed : true });
      }

      } catch (e) {
        console.log(e);
      }
  }

  renderCorrectAnswer = () => {
    const { correct, correctAnswer } = this.props;
 
    if (correct || !correctAnswer) return null;

    return <Block>
        <Text style={styles.correctAnswerHead}>Réponse:</Text>
        <Text style={styles.correctAnswerText}>{correctAnswer}</Text>
      </Block>
    ;
  }

  renderResponse = () => {
    const { correct, navigation } = this.props;
 
    const color = correct ? materialTheme.COLORS.SUCCESS : materialTheme.COLORS.ERROR;
    const iconName = correct ? 'check-circle' : 'times-circle';
    const textLabel = correct ? 'Correct' : 'Faux';
    const textStyle = correct ? styles.textSuccess : styles.textError;

    return (
      <Block flex style={styles.response}>
        <Block row>
          <Icon size={18} color={color} family="font-awesome" name={iconName} />
          <Text style={[styles.text, textStyle]}>{textLabel}</Text>
        </Block>
        {this.renderCorrectAnswer()}
        <Block center>
          <ProceedButton navigation={navigation} />
        </Block>
      </Block>
    );
  }

  render() {
    const { visible } = this.props;

    if (!visible) return null;
    
    this.playAnsweredBip();

    return (
      <Block style={styles.container}>
        {this.renderResponse()}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    width: width,
    top: screen.height / 2,
    bottom: 0,
    backgroundColor: "white",
    borderTopStartRadius: 15,
    borderTopEndRadius: 15,
    borderTopWidth: 5,
    borderTopColor: "white",
  },
  response: {
    margin: 10,
    justifyContent: 'space-between',
  },
  text: {
    fontSize: 20,
    paddingLeft: 5,
    fontWeight: "600",
    bottom: Platform.OS === 'ios' ? 3 : 8
  },
  textSuccess: {
    color: materialTheme.COLORS.SUCCESS
  },
  textError: {
    color: materialTheme.COLORS.ERROR
  },
  button: {
    marginBottom: Platform.OS === 'ios' ? 20 : 5
  },
  correctAnswerHead: {
    color: materialTheme.COLORS.CAPTION,
    fontSize: 14,
    fontWeight: "600",
    color: materialTheme.COLORS.PRIMARY,
    marginVertical: 3,
    textDecorationLine: 'underline'
  },
  correctAnswerText: {
    fontSize: 16
  },
});