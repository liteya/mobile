import React from "react";
import { StyleSheet } from "react-native";
import { Block } from 'galio-framework';

export default ButtonQuizContainer = ({ children, isDisabled  }) => (
  <Block pointerEvents={ isDisabled ? 'none' : 'auto'} style={styles.buttonContainer}>{children}</Block>
);

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 20,
    justifyContent: "space-between"
  }
});