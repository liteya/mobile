import React from 'react';
import { StyleSheet, Dimensions, Image } from 'react-native';
import { Text, Block, theme } from 'galio-framework';
import AudioPlayer from '../AudioPlayer';
import { materialTheme } from '../../constants';
import HTML from "react-native-render-html";

const { width } = Dimensions.get('screen');

export default class IllustrationFullQuiz extends React.Component {
  constructor(props) {
    super(props);
    this.audioSource = undefined;
    this.imageUri = undefined;
    this.text = undefined;
  }

  render() {
    const { audioSource, imageUri, text } = this.props;

    if (audioSource !== undefined && imageUri !== undefined && text !== undefined) { // Text, Image and Sound
      return <Block style={styles.base}>
        <AudioPlayer audioSource={audioSource} imageUri={imageUri}/> 
        <HTML containerStyle={styles.html} source={{ html: text }} />
      </Block>;
    }
    
    if (audioSource !== undefined && text !== undefined) { // Text and sound only
      return <Block style={styles.base}>
        <AudioPlayer audioSource={audioSource}/> 
        <HTML containerStyle={styles.html} source={{ html: text }} />
      </Block>;
    }
    
    if (audioSource !== undefined) { // Sound only
      return <Block style={styles.base}>
        <AudioPlayer audioSource={audioSource}/> 
      </Block>;
    }

    if (imageUri !== undefined && text !== undefined) { // Text and Image only
      return <Block style={styles.base}>
        <Image style={styles.image} source={{ uri: imageUri }} />
        <HTML containerStyle={styles.html} source={{ html: text }} />
      </Block>;
    }

    if (imageUri !== undefined) { // Image only
      return <Block style={styles.base}>
        <Image style={styles.image} source={{ uri: imageUri }} />
      </Block>;
    }

    if (text !== undefined) {
      return <Block center style={styles.base}>
        <HTML containerStyle={styles.html} source={{ html: text }} />
      </Block>
    }

    return <Block></Block>;
  }
}

const styles = StyleSheet.create({
  base: {
    marginVertical: 10,
    alignItems: 'center'
  },
  html: {
    marginVertical: 15,
    marginHorizontal: 25,
  },
  sentence: {
    color: materialTheme.COLORS.CAPTION,
  },
  image: {
    marginTop: 10,
    width: width - theme.SIZES.BASE * 5,
    height: 140
  },
});
