import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { Button as ButtonGalioFramework } from 'galio-framework';
import { materialTheme } from '../../constants';

export default class CheckAnswerButtonQuiz extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { answered, checkButtonDisabled, onPress, hidden } = this.props;
    
    if (hidden) return null;
    
    if (!answered) {
      return <ButtonGalioFramework
        shadowless
        uppercase
        style={styles.button}
        color={checkButtonDisabled ? materialTheme.COLORS.SWITCH_OFF : materialTheme.COLORS.SECONDARY }
        onPress={onPress}
        disabled={checkButtonDisabled}
      >
        Valider
      </ButtonGalioFramework>;
    }

    return null;
  }
}

const styles = StyleSheet.create({
  button: {
    marginBottom: Platform.OS === 'ios' ? 25 : 5
  },
});
