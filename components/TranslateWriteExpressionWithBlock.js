import React from 'react';
import { StyleSheet, TouchableWithoutFeedback, ScrollView } from 'react-native';
import { Block, Text } from 'galio-framework';
import AlertQuiz from './Quiz/AlertQuiz';
import IllustrationFullQuiz from './Quiz/IllustrationFullQuiz';
import CheckAnswerButtonQuiz from './Quiz/CheckAnswerButtonQuiz';
import materialTheme from '../constants/Theme';
import Context from '../store/context';

export default class TranslateWriteExpressionWithBlock extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedBlockIds: [],
      answered: false,
      checkButtonDisabled: true
    };
  }

  updateCheckButton() {
    this.setState(state => {
      return {
        checkButtonDisabled: state.selectedBlockIds.length == 0
      };
    });
  }

  unselectBlock = (blockId) => {
    this.setState(state => {
      return {
        selectedBlockIds: state.selectedBlockIds.filter(id => id !== blockId),
      };
    });
  };

  selectBlock = (blockId) => {
    this.setState(state => {
      return {
        selectedBlockIds: state.selectedBlockIds.concat(blockId),
      };
    });
  };

  renderSelectedBlocks = (blocks) => {
    const selectedBlocks = this.state.selectedBlockIds.map(blockId =>
      blocks.find(b => b.id === blockId)
    );
    return (
      <Block flex row style={styles.baseBlock}>
        {selectedBlocks.map(block => (
          <TouchableWithoutFeedback
            key={block.id}
            onPress={() => {this.unselectBlock(block.id); this.updateCheckButton();}}
          ><Block center style={[styles.baseSelectableBlock, styles.selectedBlock]}>
            <Text style={styles.selectedBlockText}>
              {block.text}
            </Text>
          </Block>
          </TouchableWithoutFeedback>
        ))}
      </Block>
    );    
  };

  renderUnselectedBlocks = (blocks) => (
    <Block flex row style={styles.baseBlockUnselect}>
      {blocks.map(block => {
        const isSelected = this.state.selectedBlockIds.includes(block.id);
        return (
          <TouchableWithoutFeedback
            key={block.id}
            onPress={() => { if (!isSelected) { 
              this.selectBlock(block.id); 
              this.updateCheckButton();
              }
            }}
          ><Block center style={[styles.baseSelectableBlock, isSelected ? styles.unselectedBlockSelected : styles.unselectedBlockNotSelected]}>
              <Text style={isSelected ? styles.unselectedBlockTextSelected : styles.unselectedBlockTextNotSelected}>
                {block.text}
              </Text>
            </Block>
          </TouchableWithoutFeedback>
        );
      })}
    </Block>
  );

  checkAnswer = (addWrongAnswerQuestionList) => {
    const { selectedBlockIds } = this.state;

    const { solution, blocks } = this.props.question.content;

    const answerInput = selectedBlockIds.map(id => blocks.find(b => b.id === id).text).join("");
    const answerCorrect = solution.map(eltSol => blocks.find(b => b.id === eltSol.id).text).join("");

    const isAnswerCorrect = answerInput === answerCorrect;
    
    if (!isAnswerCorrect) {
      addWrongAnswerQuestionList();
    }

    this.setState({
      answerCorrect: isAnswerCorrect,
      answered: true 
    });
  };

  render() {
    const { question, navigation } = this.props;

    const { blocks, solution, instruction, expression } = question.content;
    
    const correctAnswer = solution.map(eltSol => blocks.find(b => b.id === eltSol.id).text).join(" ");

    return (
      <Context.Consumer>
        {context => {
          return (
            <Block flex center style={styles.base}>

              <ScrollView
                overScrollMode='always'
                showsVerticalScrollIndicator={false}
              >
                <Block>
                  <Text h5 center color={materialTheme.COLORS.SECONDARY} bold>{instruction}</Text>
                  {expression && <IllustrationFullQuiz 
                    text={expression.text ? expression.text : undefined} 
                    audioSource={expression.audio && expression.audio.url ? expression.audio.url : undefined} 
                    imageUri={expression.image && expression.image.url ? expression.image.url : undefined} /> }
                  {this.renderSelectedBlocks(blocks)}
                  {this.renderUnselectedBlocks(blocks)}
                </Block>
                
              </ScrollView>

              <CheckAnswerButtonQuiz answered={this.state.answered} checkButtonDisabled={this.state.checkButtonDisabled} onPress={() => this.checkAnswer(context.addWrongAnswerQuestionList) }/>
              <AlertQuiz
                correct={this.state.answerCorrect}
                visible={this.state.answered}
                correctAnswer={correctAnswer}
                navigation={navigation}
              />
            </Block>);
        }}
      </Context.Consumer>
        
    );
  }
}

const styles = StyleSheet.create({
  base: {
    marginTop: 15,
  },
  baseBlock: {
    flexWrap: "wrap",
    justifyContent: "center",
    minHeight: 100,
  },
  baseBlockUnselect: {
    flexWrap: "wrap",
    justifyContent: "center",
  },
  baseSelectableBlock: {
    padding: 8,
    margin: 6,
    borderRadius: 10,
    borderWidth: 1,
  },
  selectedBlock: {
    backgroundColor: "white",
    borderColor: "white"
  },
  selectedBlockText: {
    color: "black"
  },
  unselectedBlockNotSelected: {
    backgroundColor: "white",
    borderColor: "white"
  },
  unselectedBlockSelected: {
    backgroundColor: materialTheme.COLORS.DEFAULT,
    borderColor: materialTheme.COLORS.DEFAULT
  },
  unselectedBlockTextSelected: {
    color: materialTheme.COLORS.DEFAULT
  },
  unselectedBlockTextNotSelected: {
    color: "black"
  },
});
