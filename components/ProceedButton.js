import React from 'react';
import { StyleSheet } from 'react-native';
import { Button } from 'galio-framework'
import { materialTheme } from "../constants";
import Context from '../store/context';
import { Mutation } from '@apollo/client/react/components';
import { ADD_COURSE_COMPLETED } from "../graphql/mutations";

export default class ProceedButton extends React.Component {
  constructor(props) {
    super(props);

    this.isQuestionFromWrongAnswerQuestionList = false;

    this.state = {
      isDisabled: false
    };
  }


  setCurrentQuestionOrder = (setCurrentQuestionOrder, questionOrder) => {
    setCurrentQuestionOrder(questionOrder);
  };

  render() {
    const { navigation } = this.props;

    return (
      <Context.Consumer>
        {context => {
          const course = context.getCourse(); 
          const chapter = context.getChapter();
          const user = context.getCurrentUser();
          const currentQuestionOrder = context.getCurrentQuestionOrder();
          const wrongAnswerQuestionList = context.getWrongAnswerQuestionList();
          const isQuestionFromWrongAnswerQuestionList = context.getIsQuestionFromWrongAnswerQuestionList();

          return (
            <Mutation
              mutation={ADD_COURSE_COMPLETED}
              variables={{ userId: user.id, courseId: course.id, chapterId: chapter.id}}
              onCompleted={data => context.setCurrentUser(data.addCourseCompleted)}
            >
            {addCourseCompleted => {

              let nextQuestion;
              
              let nextQuestionOrder;

              if (!isQuestionFromWrongAnswerQuestionList && (currentQuestionOrder + 1) <= course.questions.length) {
                 nextQuestionOrder = currentQuestionOrder + 1;
              } else if (wrongAnswerQuestionList.length > 0) {
                nextQuestionOrder = context.getNextWrongAnswerQuestionOrder();
                this.isQuestionFromWrongAnswerQuestionList = true;
              } 

              if (nextQuestionOrder !== undefined) {

                const question = course.questions.find(question => question.order === nextQuestionOrder);
                
                nextQuestion = () => navigation.replace(`${question.id.type}Course`, { currentQuestion: question })
              } else {
                nextQuestion = () => {
                  this.isQuestionFromWrongAnswerQuestionList = false;
                  addCourseCompleted();
                  return navigation.navigate('CoursesList', { chapterTitle: chapter.title, chapterId : chapter.id})
                }
              }

              return (<Button
                shadowless
                uppercase
                style={styles.button}
                color={materialTheme.COLORS.SECONDARY}
                disabled={this.state.isDisabled}
                onPress={() => {
                  this.setState({ isDisabled: true});
                  this.setCurrentQuestionOrder(context.setCurrentQuestionOrder, nextQuestionOrder);
                  
                  if (this.isQuestionFromWrongAnswerQuestionList) {
                    context.popWrongAnswerQuestionList();
                    context.setIsQuestionFromWrongAnswerQuestionList(this.isQuestionFromWrongAnswerQuestionList);
                  }

                  return nextQuestion();
                }}
              >
                Continuer
              </Button>);
            }}
            </Mutation>
          )
        }}
      </Context.Consumer>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    marginBottom: Platform.OS === 'ios' ? 30 : 25
  },
});