import React from 'react';
import { StyleSheet, TouchableWithoutFeedback, ScrollView, Platform } from 'react-native';
import { Block, Text } from 'galio-framework';
import materialTheme from '../constants/Theme';
import AlertQuiz from './Quiz/AlertQuiz';
import CheckAnswerButtonQuiz from './Quiz/CheckAnswerButtonQuiz';
import IllustrationFullQuiz from './Quiz/IllustrationFullQuiz';
import Context from '../store/context';

export default class Dialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedBlockIds: [],
      answered: false,
      checkButtonDisabled: true
    };
  }

  async componentDidMount() {
    const sentenceExpressionContentQuestionBlocks = this.props.question.content.expression.sentence
      .map((sentenceElt) => sentenceElt.blocks.map((sentenceEltBlocks) => sentenceEltBlocks))
      .flat();

    const selectedBlockIds = sentenceExpressionContentQuestionBlocks
    .filter(block => block.isBlank)
    .map(block => ({ "id": block.order, "fillBlockId": null })  );

    this.setState({
      selectedBlockIds: selectedBlockIds
    });
  }

  updateCheckButton() {
    this.setState(state => {
      const selectedBlockIdsNotFilled = state.selectedBlockIds.filter(item => item.fillBlockId == null);

      return {
        checkButtonDisabled: selectedBlockIdsNotFilled.length !== 0
      };
    });
  }

  isAnswerCorrect = () => {
    const { solution, blocks } = this.props.question.content;
 
    const answerCorrect = solution.map(eltSol => blocks.find(eltBlock => eltBlock.order === eltSol.id).text).join("");
    const answerInput = this.state.selectedBlockIds.map(selectedBlock => blocks.find(eltBlock => eltBlock.order === selectedBlock.fillBlockId).text).join("");

    return answerInput === answerCorrect;
  };

  checkAnswer = (addWrongAnswerQuestionList) => {
    const isAnswerCorrect = this.isAnswerCorrect();

    if (!isAnswerCorrect) {
      addWrongAnswerQuestionList();
    }

    this.setState({
      answerCorrect: isAnswerCorrect,
      answered: true 
    });
  };

  onPressUnselectOptionBlock = (blockId) => {
    this.setState(state => {
    
      const indexToRemove = state.selectedBlockIds.findIndex(item => item.fillBlockId === blockId);
      if (indexToRemove < 0) {
        return;
      }

      const block = state.selectedBlockIds[indexToRemove];
      block.fillBlockId = null;

      state.selectedBlockIds.splice(indexToRemove, 1, block);

      return {
        selectedBlockIds: state.selectedBlockIds,
      }
    });
  };

  onPressOptionBlock = (blockId) => {
    this.setState(state => {
      
      const nextIndexToFill = state.selectedBlockIds.findIndex(item => item.fillBlockId === null);
      if (nextIndexToFill < 0) {
        return;
      }

      const block = state.selectedBlockIds[nextIndexToFill];
      block.fillBlockId = blockId;

      state.selectedBlockIds.splice(nextIndexToFill, 1, block);

      return {
        selectedBlockIds: state.selectedBlockIds,
      };
    });
  };

  renderOptionBlocks = () => {

    return (
      <Block flex row style={styles.optionBlockContainer}>
        {this.props.question.content.blocks.map(block => {

        const isSelected = this.state.selectedBlockIds
          .filter(block => block.fillBlockId)
          .map(block => block.fillBlockId)
          .includes(block.order);
          
        return (
          <TouchableWithoutFeedback
            key={block.order}
            onPress={() => { if (!isSelected) this.onPressOptionBlock(block.order); this.updateCheckButton();} }
          ><Block center style={[styles.optionBaseBlock, isSelected ? styles.selectedOptionBlock : styles.optionBlock]}>
              <Text style={[styles.optionBaseBlockText, isSelected ? styles.selectedOptionBlockText : null]}>
                {block.text}
              </Text>
            </Block>
          </TouchableWithoutFeedback>
        );
        })}
      </Block>
    );
  }

  renderSentenceDialogBlocks = (sentenceEltBlocks) => {
    const optionBlocks = this.props.question.content.blocks;

    return (
      <Block flex row>
        {sentenceEltBlocks.map(sentenceBlock => {
          if (sentenceBlock.isBlank) {
            const indexFilled = this.state.selectedBlockIds
              .filter(item => item.fillBlockId != null)
              .findIndex(item => item.id === sentenceBlock.order);

            if (indexFilled >= 0) {

              const item = this.state.selectedBlockIds
                .filter(item => item.fillBlockId != null)[indexFilled];
                
              const block = optionBlocks.find(block => block.order === item.fillBlockId);

              return (
                <Block key={sentenceBlock.order} style={styles.incompleteSentenceBlock}>
                  <TouchableWithoutFeedback
                    onPress={() => {this.onPressUnselectOptionBlock(block.order); this.updateCheckButton();} }
                    key={sentenceBlock.order}
                  ><Block center style={[styles.optionBlock, styles.selectedSentenceBlock]}>
                      <Text style={[styles.selectedSentenceBlockText]}>
                        {block.text}
                      </Text>
                    </Block>
                  </TouchableWithoutFeedback>
                </Block>
              );
            }

            return (
              <Block key={sentenceBlock.order} style={styles.incompleteSentenceBlock}>
                <Block key={sentenceBlock.order} center style={styles.incompleteSentenceSubBlock}>
                  <Text>
                    {"         "}
                  </Text>
                </Block>
              </Block>
            );
          }

          return (
            <Block key={sentenceBlock.order} style={styles.incompleteSentenceBlock}>
              <Text style={styles.incompleteSentenceBlockText}>
                {sentenceBlock.text}
              </Text>
            </Block>
          );
        })}
      </Block>
    )
  };

  renderSentenceDialog = (sentenceElt) => {
    return (
      <Block key={sentenceElt.order} style={styles.sentenceDialog}>
        <Text bold={true} size={12}>{sentenceElt.name}</Text>
        {this.renderSentenceDialogBlocks(sentenceElt.blocks)}
      </Block>
    );
  };

  renderDialogBlocks = () => {
    const { expression } = this.props.question.content;

    return (
      <Block flex style={styles.incompleteSentenceBlockContainer}>
        {expression.sentence.map(sentenceElt => {
          return this.renderSentenceDialog(sentenceElt);
        })}
      </Block>
    )
  };

  render() {
    const { question, navigation } = this.props;

    const { instruction, expression, response } = question.content;

    return (
      <Context.Consumer>
        {context => {

          return (
            <Block flex center>
              <ScrollView
                overScrollMode='always'
                showsVerticalScrollIndicator={true}
              >
                <Text h5 center color={materialTheme.COLORS.SECONDARY}>{instruction}</Text>
                <IllustrationFullQuiz
                  text={expression.text ? expression.text : undefined} 
                  audioSource={expression.audio && expression.audio.url ? expression.audio.url : undefined} 
                  imageUri={expression.image && expression.image.url ? expression.image.url : undefined}
                />
                {this.renderDialogBlocks()}
                {this.renderOptionBlocks()}
              </ScrollView>
              
              <CheckAnswerButtonQuiz hidden={!this.state.answered && this.state.checkButtonDisabled} answered={this.state.answered} checkButtonDisabled={this.state.checkButtonDisabled} onPress={() => this.checkAnswer(context.addWrongAnswerQuestionList) }/>
              <AlertQuiz
                correct={this.state.answerCorrect}
                visible={this.state.answered}
                correctAnswer={response}
                navigation={navigation}
              />
            </Block>
          );
        }}
      </Context.Consumer>
        
    );
  }
}

const styles = StyleSheet.create({
  sentenceDialog: {
    padding: 2,
    borderBottomWidth: 2,
    borderBottomColor: materialTheme.COLORS.BLOCK,
  },
  incompleteSentenceBlock: {
    padding: 4,
  },
  incompleteSentenceSubBlock: {
    borderBottomWidth: 1,
    borderBottomColor: materialTheme.COLORS.QUESTION_QUIZ_COLOR_GRAY,
  },
  selectedSentenceBlockText: {
    fontSize: 14,
    padding: 2,
    paddingBottom: 2,
    paddingTop: 2,
  },
  incompleteSentenceBlockText: {
    fontSize: 14
  },
  optionBaseBlock: {
    padding: 5,
    marginHorizontal: 5,
    marginVertical: 4,
    borderRadius: 10,
    borderWidth: 1
  },
  optionBlock: {
    backgroundColor: "white",
    borderColor: "white"
  },
  optionSelectedBlock: {
    backgroundColor: materialTheme.COLORS.DEFAULT,
    borderColor: materialTheme.COLORS.DEFAULT
  },
  optionSelectedBlockText: {
    color: materialTheme.COLORS.DEFAULT
  },
  incompleteSentenceBlockContainer: {
    marginHorizontal: 20,
    marginBottom: 15,
    marginTop: 10,
    alignContent: 'center',
  },
  optionBlockContainer: {
    flexWrap: "wrap",
    justifyContent: 'center',
  },
  selectedSentenceBlock: {
    borderRadius: 10,
    borderWidth: 1,
    marginTop: -4, 
  },
  selectedOptionBlockText: {
    color: materialTheme.COLORS.DEFAULT
  },
  optionBaseBlockText: {
    color: 'black',
  },
  selectedOptionBlock: {
    backgroundColor: materialTheme.COLORS.DEFAULT,
    borderColor: materialTheme.COLORS.DEFAULT
  },
});
