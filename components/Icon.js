import React from 'react';
import { Icon } from 'galio-framework';

export default class IconExtra extends React.Component {
  render() {
    const { name, family, color, ...rest } = this.props;
  
    return <Icon name={name} family={family} color={color} {...rest} />;
  }
}
