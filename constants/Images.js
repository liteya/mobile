const LogoFull = 'https://nkoo-prod.s3.eu-west-3.amazonaws.com/image/logo.png';
const AvatarDefault = 'https://nkoo-prod.s3.eu-west-3.amazonaws.com/image/avatar_default.png';
const IconDefault = '';

export default {
  LogoFull,
  AvatarDefault,
  IconDefault
}