const QuizSuccess = 'https://d1l8lrujynwqn3.cloudfront.net/audio/quizz_success_sound.mp3';
const QuizError = 'https://d1l8lrujynwqn3.cloudfront.net/audio/quizz_error_sound.mp3';

export default {
  QuizSuccess,
  QuizError,
}