const ErrorMessageDefault = 'Une erreur innatendue est survenue. Veuillez-nous contacter à bug@nkoo.io';
const EmailInvalid = 'Veuillez saisir une adresse email valide';
const PasswordInvalid = 'Votre mot de passe doit avoir au moins 6 caractères';

export default {
  ErrorMessageDefault,
  EmailInvalid,
  PasswordInvalid
}