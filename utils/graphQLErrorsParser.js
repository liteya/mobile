const graphqlErrorsParser = (({ graphQLErrors, networkError }) => {
  let errorMessage = null;
  if (graphQLErrors) {
    errorMessage = graphQLErrors.map(error => error.message).join(". ");
    console.log(`[GraphQL error]: ${errorMessage}`);
  }
  
  if (networkError) console.log(`[GraphQL Network error]: ${networkError}`);

  return {
    displayableError: errorMessage,
    networkError: networkError ? networkError : null,
  };
});

export default graphqlErrorsParser;