import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Block, Text, theme } from 'galio-framework';

export default class Terms0fUse extends React.Component {
  render() {
    return (
      <Block flex>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.about} overScrollMode='always'>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Vous pouvez vous abonner à cette application pour accéder à l'intégralité des leçons.
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Pour s'abonner, vous devez souscrire à un abonnement mensuel à un tarif fixe et qui se renouvelle automatiquement.
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Les prix sont sujets à changement sans notification. Nous proposons occasionnellement des prix promotionnels pendant une période limitée. 
          En raison de la nature occasionnelle et promotionnelle de ces événements, nous ne sommes pas en mesure d'offrir des remises rétroactives ou des remboursements pour des achats précédents en cas de réduction de prix ou d'offre promotionnelle.
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Le paiement sera facturé via le compte iTunes ou Playstore.
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          L'abonnement se renouvelle automatiquement sauf si le renouvellement automatique est désactivé avant la fin de la période en cours.
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Les abonnements peuvent être gérés par l'utilisateur et le renouvellement automatique peut être désactivé en accédant aux paramètres du compte iTunes ou Playstore de l'utilisateur après l'achat.
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Vous pouvez désactiver le renouvellement automatique de votre abonnement via les paramètres de votre compte iTunes ou Playstore. Cependant, vous ne pouvez pas annuler l'abonnement en cours pendant sa période active.
          </Text>
        </ScrollView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  about: {    
    padding: theme.SIZES.BASE,
  },
});
