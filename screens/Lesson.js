import React from 'react';
import { StyleSheet, Dimensions, ScrollView } from 'react-native';
import { Button, Block, Text, Input, theme } from 'galio-framework';

import { Icon, Rubric } from '../components/';

const { width } = Dimensions.get('screen');

export default class Home extends React.Component {

  render() {
    return (
      <Block flex style={styles.home}>
        <Block flex style={{ 'marginTop': 20, 'marginLeft': 10 }}>
          <Text h5>C'est quoi N'KO ?</Text>
          <Text>C'est l'alphabet pour les langues mandées dont fait partit le Bambara. Il est composé de 27 lettres : 7 voyelles, 26 consonnes et une semi lettre.</Text>
          <Text h5>Les voyelles</Text>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,    
  },
  lessons: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE * 2,
  },
});
