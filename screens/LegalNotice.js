import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Block, Text, theme } from 'galio-framework';

export default class LegalNotice extends React.Component {
  render() {
    return (
      <Block flex>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.about} overScrollMode='always'>
          <Text h5 style={{ paddingTop: 9 }}>
            Nom de l'entité légale
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          IT RIVIERA CONSULTING
          </Text>
          <Text h5 style={{ paddingTop: 9 }}>
            Siège social
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          9 rue d’estienne d’orves 91220 Brétigny-sur-Orge
          </Text>
          <Text h5 style={{ paddingTop: 9 }}>
            Représentant légal
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Sedomon Landry Alagbe
          </Text>
          <Text h5 style={{ paddingTop: 9 }}>
            Immatriculation
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Numéro d’immatriculation de l’entreprise : 827 619 594 00019
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Code APE ou NAF: 6202A
          </Text>
        </ScrollView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  about: {    
    padding: theme.SIZES.BASE,
  },
});
