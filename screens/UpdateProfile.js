import React from 'react';
import { StyleSheet, Dimensions, KeyboardAvoidingView, Alert, Platform } from 'react-native';
import { Block, Button, Input, Text, theme } from 'galio-framework';
import { FontAwesome } from '@expo/vector-icons';
import { Mutation } from '@apollo/client/react/components';
import { materialTheme, messages } from '../constants';
import { UPDATE_USER } from "../graphql/mutations";
import Context from '../store/context';
import graphqlErrorsParser from '../utils/graphQLErrorsParser.js';
import validator from 'validator';

const { width, height } = Dimensions.get('window');

export default class UpdateProfile extends React.Component {
  state = {
    name: '',
    active: {
      name: false,
    }
  }

  handleChange = (name, value) => {
    this.setState({ [name]: value });

    if (this.state.errorMessage) {
      this.setState({
        errorMessage: '',
      })
    }
  }

  toggleActive = (name) => {
    const { active } = this.state;
    active[name] = !active[name];

    this.setState({ active });
  }

  storeToken = async (saveToken, token) => {
    saveToken(token)
      .then(() => {
        this.props.navigation.goBack();
      })
      .catch(error => {
        this.setState({ error })
      });
  };

  handleUpdateUserError = async (error) => {
    const graphqlErrors = graphqlErrorsParser(error);

    if (graphqlErrors.displayableError) {
      this.setState({
        errorMessage: graphqlErrors.displayableError,
      })
    }

    if (graphqlErrors.networkError) {
      this.setState({
        errorMessage: messages.ErrorMessageDefault,
      })
    }
  };

  renderErrorMessage = () => {
    if (this.state.errorMessage) {
      return <Text style={{ color: materialTheme.COLORS.ERROR }} size={theme.SIZES.FONT}>{this.state.errorMessage}</Text>;
    }

    return null;
  }

  render() {
    const { navigation } = this.props;

    return (
      <Block flex middle style={[styles.signin, {flex: 1, paddingTop: theme.SIZES.BASE * 2}]}>
        <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} enabled>
          <Context.Consumer>
            {context => (
              <Block flex>            
                <Block center>
                  <Input
                    borderless
                    color="black"
                    label="Nom"
                    defaultValue={context.getCurrentUser().name}
                    autoCapitalize="none"
                    bgColor='transparent'
                    onBlur={() => this.toggleActive('name')}
                    onFocus={() => this.toggleActive('name')}
                    onChangeText={text => this.handleChange('name', text)}
                    style={[styles.input, this.state.active.name ? styles.inputActive : null]}
                  />
                  {this.renderErrorMessage()}
                </Block>
                <Block flex top style={{ marginTop: 20 }}>
                  <Mutation
                      mutation={UPDATE_USER}
                      variables={{ id: context.getCurrentUser().id, name: this.state.name }}
                      onCompleted={data => context.setCurrentUser(data.updateUser) && navigation.navigate('Settings')}
                      onError={error => this.handleUpdateUserError(error)}  
                    >
                      {updateUser => (
                        <Button
                          shadowless
                          uppercase
                          color={this.state.name ? materialTheme.COLORS.BUTTON_COLOR : materialTheme.COLORS.DEFAULT }
                          style={{ height: 48 }}
                          onPress={() => updateUser()}
                          disabled={!(this.state.name)}
                        >
                        Modifier mon profil
                        </Button>
                      )}
                    </Mutation>
                </Block>
              </Block>
            )}

          </Context.Consumer>
        </KeyboardAvoidingView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  signin: {        
    marginTop: Platform.OS === 'ios' ? 60 : 0,
  },
  input: {
    width: width * 0.9, 
    borderRadius: 0,
    borderBottomWidth: 1,
    borderBottomColor: materialTheme.COLORS.PLACEHOLDER,
  },
  inputActive: {
    borderBottomColor: "black",
  },
});
