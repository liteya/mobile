import React from 'react';
import { StyleSheet, ActivityIndicator, ScrollView, Image, Dimensions } from 'react-native';
import { Block, Text, Button, theme } from 'galio-framework';
import { materialTheme } from '../constants';
import iap from '../store/iap';
import Context from '../store/context';
import { GET_ONE_USER } from "../graphql/queries";
import { Query } from '@apollo/client/react/components';
import HTML from "react-native-render-html";

const { width, height } = Dimensions.get('screen');
export default class Subscriptions extends React.Component {
  state = {
    buyProductProcessStatus: 'READY'
  };

  handleBuyProduct = async (product) => {
    const { navigation } = this.props;

    if (!product) {
      return navigation.navigate('ContactUs');
    }

    this.setState({
      buyProductProcessStatus: 'PROCESSING'
    });
    await iap.buy(product.sku);
    this.setState({
      buyProductProcessStatus: 'FINISHED'
    });
  }

  render() {
    const { navigation } = this.props;
    const htmlContentPrice = `<div style="fontWeight: bold; font-size: 20">Abonnement de 1 mois à <span style="color: #0D1467; font-size: 22">9,99€ sans engagement</span>. Tu peux arrêter ton abonnement à tout moment.</div>`;
    
    const htmlContentAvantageIll = `<div style="color: #EE7056; fontWeight: bold; font-size: 19">Accède de manière illimitée et complète à toutes les leçons.</div>`;
    const htmlContentAvantageProfs = `<div style="fontWeight: bold; font-size: 13"><span style="color: #0D1467; font-size: 16">- </span>Accès à coût réduit aux services de nos professeurs et locuteurs natifs pour des échanges réels et personnalisés (cours privés, groupes WhatsApp)</div>`;

    return (
      <Context.Consumer>
        {context => {
          const currentUser = context.getCurrentUser();
          const premiumAccessActivated = currentUser && currentUser.premiumAccess ? currentUser.premiumAccess.activated : false;
          
          if (premiumAccessActivated) {
            return (
              <Block flex center style={styles.base}>
                <Button
                  shadowless
                  uppercase
                  color={materialTheme.COLORS.SECONDARY}
                  style={styles.button}
                  onPress={() => navigation.navigate('Home')}
                  >
                  Continuer à apprendre
                </Button>
              </Block>
            )
          }

          if (this.state.buyProductProcessStatus === 'READY') {
            const product = iap.productsForSale.length > 0 ? iap.productsForSale[0] : iap.activeProducts[0];
            return (
              <Block flex center style={styles.base}>
                  <Block>
                    <Text size={16} bold center color={materialTheme.COLORS.SECONDARY}>{'Continue ton apprentissage avec la formule Premium'}</Text>
                  </Block>

                  <ScrollView
                    overScrollMode='always'
                    showsVerticalScrollIndicator={false}
                  >
                    <Block flex center>
                      <Image source={{ uri: 'https://nkoo-prod.s3.eu-west-3.amazonaws.com/image/subscriptions_illustration.jpg' }} style={styles.image} />

                      <Block style={styles.blockList}>
  
                        <Block center style={styles.list}>
                          <HTML source={{ html: htmlContentAvantageIll }} />  
                        </Block>

                        <Block center style={[styles.listPrice]}>
                          <HTML source={{ html: htmlContentPrice }} />
                        </Block>                      
                      </Block>

                    </Block>
                  </ScrollView>

                  <Button
                    shadowless
                    uppercase
                    color={materialTheme.COLORS.SECONDARY}
                    style={styles.button}
                    onPress={() => this.handleBuyProduct(product)}
                    >
                    Je m'abonne
                  </Button>
              </Block>
            );
          }

          if (this.state.buyProductProcessStatus === 'PROCESSING') {
            return (
              <Block flex center style={styles.base}>
                <ActivityIndicator size="large" color={materialTheme.COLORS.PRIMARY} />
              </Block>
            )
          }

          if (this.state.buyProductProcessStatus === 'FINISHED') {
            return (
              <Block flex middle style={styles.base}>
                <Query
                  query={GET_ONE_USER}
                  variables={{ id: currentUser.id }}
                  onCompleted={data => context.setCurrentUser(data.User)}
                  fetchPolicy={'no-cache'}
                >
                  {({ loading, error, data }) => {
                    console.log(data);
                    if (loading || error) return <ActivityIndicator size="large" color={materialTheme.COLORS.PRIMARY} />
                    return (
                      <Button
                      shadowless
                      uppercase
                      color={materialTheme.COLORS.SECONDARY}
                      style={styles.button}
                      onPress={() => navigation.navigate('Home')}
                      >
                        Retour aux leçons
                      </Button>
                    )
                  }}
                </Query>
              </Block>
            );
          }

        }}
      </Context.Consumer>
    );
  }
}

const styles = StyleSheet.create({
  base: {    
    marginVertical: 10,
    justifyContent: 'space-between' 
  },
  blockList: {    
    margin: 5,
    padding: 5,
  },
  list: {
    flexWrap: 'nowrap',    
    flexDirection: 'row',
    marginVertical: 10,
  },
  listPrice: {
    flexWrap: 'wrap',    
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 18,
  },
  listIcon: {
    marginRight: 8,
  },
  image: {
    borderRadius: 3,
    height: 200,
    width: width - theme.SIZES.BASE * 3,
  },
  button: {    
    marginBottom: Platform.OS === 'ios' ? 30 : 10,
  },
});
