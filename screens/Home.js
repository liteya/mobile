import React from 'react';
import { StyleSheet, Dimensions, ScrollView, ActivityIndicator } from 'react-native';
import { Block, theme } from 'galio-framework';
import Context from '../store/context';
import { Chapter } from '../components/';
import { GET_ONE_LANGUAGE } from "../graphql/queries";
import { Query } from '@apollo/client/react/components';
import { materialTheme } from '../constants';
import { FontAwesome } from '@expo/vector-icons';

const { width, height } = Dimensions.get('screen');
const DEFAULT_LEVEL = 0;

const CHAPTER_COMPLETED = 'COMPLETED';
const CHAPTER_OPEN = 'OPEN';
const CHAPTER_LOCKED = 'LOCKED';
export default class Home extends React.Component {

  getChapterStatus(chapter, allChapters, currentLanguage) {
    const isChapterCompleted = currentLanguage.levels[DEFAULT_LEVEL].chapters.completed.find(elt => elt.id === chapter.id.id);

    if (isChapterCompleted) {
      return CHAPTER_COMPLETED;
    }

    if (chapter.id.id === allChapters[0].id.id) {
      return CHAPTER_OPEN;
    }

    const prevChapterOrder = chapter.order - 1;
    const prevChapter = allChapters.find(eltChapter => eltChapter.order === prevChapterOrder);
        
    const isPrevChapterCompleted = currentLanguage.levels[DEFAULT_LEVEL].chapters.completed.find(elt => elt.id === prevChapter.id.id);
    if (isPrevChapterCompleted) {
      return CHAPTER_OPEN;
    }

    return CHAPTER_LOCKED;
  }

  render() {
    const { navigation } = this.props;
    
    return (
      <Block flex center style={styles.home}>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.chapters}>
          <Block flex>
            <Context.Consumer>
              {context => {
                if (context.getCurrentUser()) {
                  const currentLanguage = context.getCurrentUser().languages.find(language => language.isCurrentLearning === true);
                  if (currentLanguage) {
                    return (
                    <Query
                      query={GET_ONE_LANGUAGE}
                      variables={{ id: currentLanguage.id.id }}
                    >
                      {({ loading, error, data }) => {
                        if (loading || error) return <ActivityIndicator size="large" color={materialTheme.COLORS.PRIMARY} />
                        const chapters = JSON.parse(JSON.stringify(data.language.levels[DEFAULT_LEVEL].id.chapters));
        
                        return (
                          chapters
                            .sort((a, b) => a.order - b.order)
                            .map(chapter => <Chapter key={chapter.id.id} chapter={chapter.id} status={this.getChapterStatus(chapter, chapters, currentLanguage)} />)
                        )
                      }}
                    </Query>
                  )}

                  return (
                    <Block flex style={{alignItems: 'center', justifyContent: 'flex-end', marginBottom: 30}}>
                      <FontAwesome.Button
                        name="plus-circle"
                        size={16}
                        backgroundColor={materialTheme.COLORS.SECONDARY}
                        style={{ width : width / 1.1, height: height / 16, justifyContent: 'center'}}
                        onPress={() => navigation.navigate('AvailableCoursesList')}
                      >
                        {'Apprendre une nouvelle langue'.toUpperCase()}
                      </FontAwesome.Button>
                    </Block>
                  );
                }
              }}
            </Context.Consumer>
          </Block>
        </ScrollView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,    
  },
  chapters: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE * 2,
  },
});
