import React from 'react';
import { ScrollView, StyleSheet, Image, Dimensions, FlatList, TouchableOpacity } from 'react-native';
import { Block, Text, theme } from 'galio-framework';
import { Images, materialTheme } from '../constants';
import { FontAwesome } from '@expo/vector-icons';
import { ListItem } from 'react-native-elements';
import Context from '../store/context';
import { Mutation } from '@apollo/client/react/components';
import { ACTIVATE_CURRENT_LEARNING_LANGUAGE } from "../graphql/mutations";

const { width, height } = Dimensions.get('screen');

export default class MyCourses extends React.Component {

  render() {
    const { navigation } = this.props;
    
    return (
      <Block flex>
        <Context.Consumer>
          {context => (
            context.getCurrentUser().languages.map((l) => {
              return (
                <Mutation
                  mutation={ACTIVATE_CURRENT_LEARNING_LANGUAGE}
                  variables={{ userId: context.getCurrentUser().id, languageId: l.id.id }}
                  onCompleted={data => context.setCurrentUser(data.activateCurrentLearningLanguage)}
                  key={l.id.id}
                >
                  {activateCurrentLearningLanguage => (
                    
                    <ListItem
                      key={l.id.id}
                      onPress={() => activateCurrentLearningLanguage() && navigation.navigate('Home')}
                      bottomDivider
                    >
                      <ListItem.Content>
                        <ListItem.Title>{l.id.title}</ListItem.Title>
                      </ListItem.Content>
                    </ListItem>
                  )}
                </Mutation>
              )
            })
          )}
        </Context.Consumer>

        <Block flex style={{alignItems: 'center', justifyContent: 'flex-end', marginBottom: 30}}>
          <FontAwesome.Button
            name="plus-circle"
            size={16}
            backgroundColor={materialTheme.COLORS.SECONDARY}
            style={{ width : width / 1.1, height: height / 16, justifyContent: 'center'}}
            onPress={() => navigation.navigate('AvailableCoursesList')}
          >
            {'Apprendre une nouvelle langue'.toUpperCase()}
          </FontAwesome.Button>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  course: {    
    padding: theme.SIZES.BASE,
  },
  logoCourse: {
    width: width / 9,
    height: height / 19,
    marginBottom: 10,
    alignSelf: 'center',
  }
});
