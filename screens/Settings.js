import React from 'react';
import { StyleSheet, Switch, FlatList, Platform, TouchableOpacity, View } from "react-native";
import { Block, Text, theme, Icon } from "galio-framework";
import Context from '../store/context';
import materialTheme from '../constants/Theme';
import { Alert } from 'react-native';

export default class Settings extends React.Component {
  state = {};

  toggleSwitch = switchNumber => this.setState({ [switchNumber]: !this.state[switchNumber] });

  onPressLogout = (removeToken, removeCurrentUser) => {
    removeToken();
    removeCurrentUser();
  };

  renderItem = ({ item }) => {
    const { navigate } = this.props.navigation;

    switch(item.type) {
      case 'switch': 
        return (
          <Block row middle space="between" style={styles.rows}>
            <Text size={14}>{item.title}</Text>
            <Switch
              onValueChange={() => this.toggleSwitch(item.id)}
              ios_backgroundColor={materialTheme.COLORS.SWITCH_OFF}
              thumbColor={Platform.OS === 'android' ? materialTheme.COLORS.SWITCH_OFF : null}
              trackColor={{ false: materialTheme.COLORS.SWITCH_OFF, true: materialTheme.COLORS.SWITCH_ON }}
              value={this.state[item.id]}
            />
          </Block>
        );
      case 'button': 
        return (
          <Block style={styles.rows}>
            <TouchableOpacity onPress={() => (item.id !== 'Payment') && navigate(item.id)}>
              <Block row middle space="between" style={{ paddingTop: 7 }}>
                <Text size={14}>{item.title}</Text>
                <Icon name="angle-right" family="font-awesome" style={{ paddingRight: 5 }} />
              </Block>
            </TouchableOpacity>
          </Block>);
      default:
        break;
    }
  }

  render() {
    const myProfile = [
      { title: "Modifier mon profil", id: "UpdateProfile", type: "button" },
    ];

    const notifications = [
      { title: "Notifications", id: "Notifications", type: "switch" },
    ];

    const payment = [
      { title: "Gestion des moyens de paiement", id: "Payment", type: "button" },
    ];
    
    const information = [
      { title: "Politique de confidentialité", id: "Privacy", type: "button" },
      { title: "Conditions d'utilisation", id: "TermsOfUse", type: "button" },
      { title: "A propos", id: "About", type: "button" },
      { title: "Mentions légales", id: "LegalNotice", type: "button" },
      { title: "Contactez-nous", id: "ContactUs", type: "button" },
    ];

    const logout = [
      { title: "Déconnexion", id: "Logout", type: "default" },
    ];

    return (
      <View
        style={styles.settings}>

        <Block center style={styles.title}>
          <Text bold size={theme.SIZES.BASE} style={{ paddingBottom: 5 }}>
            {'Mon profil'.toUpperCase()}
          </Text>
        </Block>

        <FlatList
          style={styles.list}
          data={myProfile}
          keyExtractor={(item, index) => item.id}
          renderItem={this.renderItem}
        />

        {/*<Block center style={styles.title}>
          <Text bold size={theme.SIZES.BASE} style={{ paddingBottom: 5 }}>
            {'Notifications'.toUpperCase()}
          </Text>
        </Block>

        <FlatList
          style={styles.list}
          data={notifications}
          keyExtractor={(item, index) => item.id}
          renderItem={this.renderItem} 
          />

        <Block center style={styles.title}>
          <Text uppercase bold size={theme.SIZES.BASE} style={{ paddingBottom: 5 }}>
            {'Mes moyens de paiement'.toUpperCase()}
          </Text>
        </Block>

        <FlatList
          style={styles.list}
          data={payment}
          keyExtractor={(item, index) => item.id}
          renderItem={this.renderItem} />*/}

        <Block center style={styles.title}>
          <Text bold size={theme.SIZES.BASE} style={{ paddingBottom: 5 }}>
            {'Informations'.toUpperCase()}
          </Text>
        </Block>
        <FlatList
          style={styles.list}
          data={information}
          keyExtractor={(item, index) => item.id}
          renderItem={this.renderItem}
        />

        <FlatList
          style={styles.list}
          data={logout}
          renderItem={() =>
            <Block style={styles.rows}>
              <Context.Consumer>
                {context => (
                  <TouchableOpacity onPress={() => this.onPressLogout(context.removeToken, context.removeCurrentUser)}>
                    <Block row middle space="between" style={{ paddingTop: 7 }}>
                      <Text size={14}>{'Se déconnecter'}</Text>
                      <Icon name="sign-out" family="font-awesome" style={{ paddingRight: 5 }} />
                    </Block>
                  </TouchableOpacity>
                )}
                </Context.Consumer>
            </Block>
          }
        />
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  settings: {
    paddingVertical: theme.SIZES.BASE / 3,
  },
  list: {
    backgroundColor: 'white',
    marginVertical: theme.SIZES.BASE / 3,
  },
  title: {
    paddingTop: theme.SIZES.BASE,
    paddingBottom: theme.SIZES.BASE / 2,
  },
  rows: {
    height: theme.SIZES.BASE * 2,
    paddingHorizontal: theme.SIZES.BASE,
    marginBottom: theme.SIZES.BASE / 2,
  }
});
