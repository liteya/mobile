import React from 'react';
import { StyleSheet, Dimensions, Image, Platform} from 'react-native';
import { Block, Text, theme } from 'galio-framework';

import { Images } from '../constants';
import { HeaderHeight } from "../constants/utils";
import materialTheme from '../constants/Theme';
import Context from '../store/context';

const { width, height } = Dimensions.get('screen');

export default class Profile extends React.Component {
  render() {
    return (
      <Context.Consumer>
        {context => (
          <Block>
            <Block style={styles.header} />
            <Image style={styles.avatar} source={{ uri: Images.AvatarDefault }}/>
            <Block style={{ marginVertical: theme.SIZES.BASE * 4}}>
              <Text center style={{ fontWeight: 'bold' }} size={theme.SIZES.FONT * 1.4}>
                {context.getCurrentUser() && context.getCurrentUser().name}
              </Text>
              <Text center color={theme.COLORS.INPUT} size={theme.SIZES.FONT}>
              {context.getCurrentUser() && context.getCurrentUser().email}
              </Text>
            </Block>
          </Block>
        )}
      </Context.Consumer>
    );
  }
}

const styles = StyleSheet.create({
  profile: {
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
  },
  header:{
    backgroundColor: materialTheme.COLORS.PRIMARY,
    height: height / 6,
  },
  avatar: {
    width: width / 3.18,
    height: height / 7,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom:10,
    alignSelf: 'center',
    position: 'absolute',
    marginTop: 80
  },
  body:{
    marginTop: 40,
  }
});
