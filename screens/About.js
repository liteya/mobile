import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Block, Text, theme } from 'galio-framework';

export default class About extends React.Component {
  render() {
    return (
      <Block flex>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.about} overScrollMode='always'>
          <Text size={16} color='rgba(0, 0, 0, 0.5)' style={{ paddingTop: 9 }}>
            Sedomon Landry Alagbe, afro-descendant, s'est donné pour mission de concevoir des produits et services afin
            de valoriser, promouvoir et faciliter la transmission des langues africaines.
          </Text>
        </ScrollView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  about: {    
    padding: theme.SIZES.BASE,
  },
});
