import React from 'react';
import { Alert, Dimensions, StyleSheet, KeyboardAvoidingView, Platform } from 'react-native';
import { Block, Button, Input, Text, theme } from 'galio-framework';
//import Icon from 'react-native-vector-icons/FontAwesome';
import { FontAwesome } from '@expo/vector-icons';
import { Mutation } from '@apollo/client/react/components';
import { materialTheme, messages } from '../constants/';
import { SIGN_UP } from "../graphql/mutations";
import Context from '../store/context';
import graphqlErrorsParser from '../utils/graphQLErrorsParser.js';
import validator from 'validator';
import iap from '../store/iap';

const { height, width } = Dimensions.get('window');

export default class SignUp extends React.Component {
  state = {
    email: '',
    password: '',
    active: {
      email: false,
      password: false,
    },
    errorMessage: ''
  }

  handleChange = (name, value) => {
    this.setState({ [name]: value });

    if (this.state.errorMessage) {
      this.setState({
        errorMessage: '',
      })
    }
    
  }

  toggleActive = (name) => {
    const { active } = this.state;
    active[name] = !active[name];

    this.setState({ active });
  }

  storeToken = async (saveToken, token) => {
    saveToken(token)
      .then(() => {
        this.props.navigation.navigate('App');
      })
      .catch(error => {
        this.setState({ error })
      });
  };

  setIAPUserId = async (user) => {
    iap.setUserId(user.id);
    await iap.getActiveProducts();
    await iap.getProductsForSale();
  };

  handleSignUpError = async (error) => {
    const graphqlErrors = graphqlErrorsParser(error);

    if (graphqlErrors.displayableError) {
      this.setState({
        errorMessage: graphqlErrors.displayableError,
      })
    }

    if (graphqlErrors.networkError) {
      this.setState({
        errorMessage: messages.ErrorMessageDefault,
      })
    }
  };

  handleSignup = signup => {
    if (!validator.isEmail(this.state.email)) {
      this.setState({
        errorMessage: messages.EmailInvalid
      })

      return;
    }

    if (this.state.password.length < 6) {
      this.setState({
        errorMessage: messages.PasswordInvalid
      })

      return;
    }

    signup();
  };

  renderErrorMessage = () => {
    if (this.state.errorMessage) {
      return <Text style={{ color: materialTheme.COLORS.ERROR }} size={theme.SIZES.FONT}>{this.state.errorMessage}</Text>;
    }

    return null;
  }

  render() {
    const { navigation } = this.props;
    
    return (
      <Block flex middle style={[styles.signin, {flex: 1, paddingTop: theme.SIZES.BASE * 5}]}>
        <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} enabled>
          
          {/*<Block middle>
            <Block style={{ marginBottom: theme.SIZES.BASE * 1, marginTop: Platform.OS === "ios" ? theme.SIZES.BASE * 5 : theme.SIZES.BASE * 2 }}>
              <FontAwesome.Button
                name="facebook"
                backgroundColor={materialTheme.COLORS.FACEBOOK}
                style={{ width: width / 1.2, height: height / 16, justifyContent: 'center' }}
                onPress={() => Alert.alert('Not implemented')}
              >
                CONNECTION AVEC FACEBOOK
              </FontAwesome.Button>
            </Block>

            <Block>
              <FontAwesome.Button
                name="google"
                backgroundColor={materialTheme.COLORS.GOOGLE}
                style={{ width : width / 1.2, height: height / 16, justifyContent: 'center' }}
                onPress={() => Alert.alert('Not implemented')}
              >
                CONNECTION AVEC GOOGLE
              </FontAwesome.Button>
            </Block>
          </Block>
          <Block middle style={{ paddingVertical: theme.SIZES.BASE * 1}}>
            <Text center color={theme.COLORS.INPUT} size={theme.SIZES.FONT * 0.875}>
              ou
            </Text>
          </Block>
        */}
          <Block flex={1} center space="between">
            <Block center>
              <Input
                bgColor='transparent'
                placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
                borderless
                color="black"
                type="email-address"
                placeholder="Veuillez saisir votre email"
                autoCapitalize="none"
                style={[styles.input, this.state.active.email ? styles.inputActive : null]}
                onChangeText={text => this.handleChange('email', text)}
                onBlur={() => this.toggleActive('email')}
                onFocus={() => this.toggleActive('email')}
              />
              <Input
                bgColor='transparent'
                placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
                borderless
                color="black"
                password
                viewPass
                placeholder="Veuillez saisir votre mot de passe"
                iconColor={theme.COLORS.INPUT}
                style={[styles.input, this.state.active.password ? styles.inputActive : null]}
                onChangeText={text => this.handleChange('password', text)}
                onBlur={() => this.toggleActive('password')}
                onFocus={() => this.toggleActive('password')}
              />
            </Block>
            {this.renderErrorMessage()}
            <Block flex top style={{ marginTop: 20 }}>
              <Context.Consumer>
                {context => (
                  <Mutation
                    mutation={SIGN_UP}
                    variables={{ input: { email: this.state.email, password: this.state.password } }}
                    onCompleted={data => this.storeToken(context.saveToken, data.signup.token) && context.setCurrentUser(data.signup.user) && this.setIAPUserId(data.login.user)}  
                    onError={error => this.handleSignUpError(error)}
                  >
                    {signup => (
                      <Button
                        shadowless
                        style={{ height: 48, marginBottom: 30 }}
                        color={this.state.email && this.state.password ? materialTheme.COLORS.BUTTON_COLOR : materialTheme.COLORS.DEFAULT }
                        uppercase
                        onPress={() => this.handleSignup(signup)}
                        disabled={!(this.state.email && this.state.password)}
                      >
                        Je m'inscris
                      </Button>
                    )}
                  </Mutation>
                )}
              </Context.Consumer>
              <Button color="transparent" shadowless onPress={() => navigation.navigate('Sign In')}>
                <Text center color={materialTheme.COLORS.SECONDARY} size={theme.SIZES.FONT} style={{textTransform: 'uppercase', textDecorationLine: 'underline'}}>
                  Vous avez déjà un compte? Connectez-vous
                </Text>
              </Button>
            </Block>
          </Block>
        </KeyboardAvoidingView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  signup: {
    marginTop: Platform.OS === 'ios' ? 60 : 0,
  },
  input: {
    width: width * 0.9,
    borderRadius: 0,
    borderBottomWidth: 1,
    borderBottomColor: materialTheme.COLORS.PLACEHOLDER,
  },
  inputActive: {
    borderBottomColor: "black",
  },
});
