import React from 'react';
import { FillExpressionWithBlock } from '../../components';

export default class FillExpressionWithBlockCourse extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { navigation, route } = this.props;
    const { currentQuestion } = route.params;

    return <FillExpressionWithBlock question={currentQuestion.id} navigation={navigation}/>;
  }
}
