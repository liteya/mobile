import React from 'react';
import { PairingExpression } from '../../components';

export default class PairingExpressionCourse extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { navigation, route } = this.props;
    const { currentQuestion } = route.params;

    return <PairingExpression question={currentQuestion.id} navigation={navigation}/>;
  }
}
