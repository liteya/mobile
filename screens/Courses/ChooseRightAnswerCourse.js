import React from 'react';
import { ChooseRightAnswerAmongOptions } from '../../components';

export default class ChooseRightAnswerCourse extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { navigation, route } = this.props;
    const { currentQuestion } = route.params;

    return <ChooseRightAnswerAmongOptions questionOrder={currentQuestion.order} question={currentQuestion.id} navigation={navigation} />;
  }
}
