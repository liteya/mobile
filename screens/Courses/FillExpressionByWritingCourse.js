import React from 'react';
import { FillExpressionByWriting } from '../../components';
import Context from '../../store/context';
import { Mutation } from '@apollo/client/react/components';
import { ADD_COURSE_COMPLETED } from "../../graphql/mutations";
export default class FillExpressionByWritingCourse extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { navigation, route } = this.props;
    const { currentQuestion } = route.params;

    return <FillExpressionByWriting question={currentQuestion.id} navigation={navigation}/>;
  }
}
