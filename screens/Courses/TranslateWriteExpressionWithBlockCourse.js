import React from 'react';
import { TranslateWriteExpressionWithBlock } from '../../components';

export default class TranslateWriteExpressionWithBlockCourse extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { navigation, route } = this.props;
    const { currentQuestion } = route.params;

    return <TranslateWriteExpressionWithBlock question={currentQuestion.id} navigation={navigation}/>;
  }
}
