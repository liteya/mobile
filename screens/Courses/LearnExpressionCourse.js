import React from 'react';
import { LearnExpression } from '../../components';

export default class LearnExpressionCourse extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { navigation, route } = this.props;
    const { currentQuestion } = route.params;

    return <LearnExpression question={currentQuestion.id} navigation={navigation} />;
  }
}
