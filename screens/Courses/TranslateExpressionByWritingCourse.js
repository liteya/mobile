import React from 'react';
import { TranslateExpressionByWriting } from '../../components';

export default class TranslateExpressionByWritingCourse extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { navigation, route } = this.props;
    const { currentQuestion } = route.params;

    return <TranslateExpressionByWriting question={currentQuestion.id} navigation={navigation}/>;
  }
}
