import React from 'react';
import { Dialog } from '../../components';

export default class DialogCourse extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { navigation, route } = this.props;
    const { currentQuestion } = route.params;

    return <Dialog question={currentQuestion.id} navigation={navigation} />;
  }
}
