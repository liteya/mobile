import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';

import { Block, Text, theme } from 'galio-framework';

export default class Privacy extends React.Component {
  render() {
    return (
      <Block flex>
        <ScrollView
          overScrollMode='always'
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.privacy}
          >
          <Text h5>
            Protection des données personnelles
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
            Nous utilisons vos données pour fournir et améliorer le service. En utilisant le Service, vous acceptez la collecte et l’utilisation d’informations conformément à cette politique. La présente politique de confidentialité fait intégralement partie des Termes et Conditions d'utilisation du Service.
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
            Tout est mis en œuvre pour garantir la sécurité de ces données. Pour toute question relative à vos données, de leurs usages et protections, veuillez adresser un courrier électronique à privacy@nkoo.io.
          </Text>

          <Text h5 style={{ paddingTop: 9 }}>
            Types de données collectées :
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
            Lors de l’utilisation de notre service, nous pouvons vous demander de nous fournir certaines informations personnellement identifiables qui peuvent être utilisées pour vous contacter ou vous identifier (“Données personnelles”). Les informations personnellement identifiables peuvent inclure, sans toutefois s’y limiter: 
            Nom et prénom, E-mail, Adresse, Téléphone, Pays, Nom d’Utilisateur, Mot de passe, Langue, Données d’utilisation, Autre informations relatives à votre profil
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
            Les Données personnelles peuvent être librement fournies par l’Utilisateur, ou, en cas de Données d’utilisation, collectées automatiquement lorsque vous utilisez cette plateforme ou www.nkoo.io.
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>  
            Sauf indication contraire, toutes les Données demandées par cette plateforme ou nkoo.io sont obligatoires et leur absence peut rendre impossible la fourniture des Services par cette plateforme ou www.nkoo.io/. Dans le cas où cette plateforme ou www.nkoo.io précise que certaines Données ne sont pas obligatoires, les Utilisateurs sont libres de ne pas les communiquer sans entraîner de conséquences sur la disponibilité ou le fonctionnement du Service.
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
            Toute utilisation des Cookies – ou d’autres outils de suivi – par cette plateforme ou www.nkoo.io ou par les propriétaires de services tiers utilisés par https://www.nkoo.io vise à fournir le Service demandé par l’Utilisateur, outre les autres finalités décrites dans le présent document et dans la Politique relative aux cookies, si elle est disponible.
            Les Utilisateurs sont responsables de toute Donnée personnelle de tiers obtenue, publiée ou communiquée par l’intermédiaire de cette plateforme ou https://www.nkoo.io et confirment qu’ils obtiennent le consentement du tiers pour fournir les Données au Propriétaire. 
          </Text>
          
          <Text h5 style={{ paddingTop: 9 }}>
            Données d’utilisation
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Nous pouvons également collecter des informations sur les modalités d’accès et d’utilisation du service (“données d’utilisation”). Ces données d’utilisation peuvent inclure des informations telles que l’adresse de protocole Internet de votre ordinateur (par exemple, l’adresse IP), le type de navigateur, la version du navigateur, les pages de notre service que vous visitez, l’heure et la date de votre visite, le temps passé sur ces pages, identificateurs d’appareil et autres données de diagnostic.
          </Text>

          <Text h5 style={{ paddingTop: 9 }}>
            Utilisation des données 
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Nkoo utilise les données collectées à diverses fins:
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>- Fournir et gérer le service</Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>- Pour vous informer des modifications apportées à notre service</Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>- Pour vous permettre de participer aux fonctionnalités interactives de notre service lorsque vous le souhaitez</Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>- Assurer l’assistance et le support client</Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>- Fournir des analyses ou des informations précieuses afin que nous puissions améliorer le service</Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>- Pour surveiller l’utilisation du service</Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>- Détecter, prévenir et résoudre les problèmes techniques</Text>


          <Text h5 style={{ paddingTop: 9 }}>
          Transfert de données
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Vos informations, y compris vos données personnelles, peuvent être transférées et conservées sur des ordinateurs situés en dehors de votre état, province, pays ou autre juridiction gouvernementale où les lois sur la protection des données peuvent différer de celles de votre juridiction.       
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Si vous résidez hors de France et que vous choisissez de nous fournir des informations, veuillez noter que nous transférons les données, y compris les données à caractère personnel, vers la France et les traitons là-bas.       
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Votre consentement à cette politique de confidentialité, suivi de votre soumission de telles informations, représente votre acceptation de ce transfert.     
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Nkoo prendra toutes les mesures raisonnablement nécessaires pour garantir le traitement sécurisé de vos données, conformément à la présente politique de confidentialité. Aucun transfert de vos données personnelles ne sera effectué vers une organisation ou un pays, à moins que des contrôles adéquats ne soient en place, y compris: sécurité de vos données et autres informations personnelles.
          </Text>

          <Text h5 style={{ paddingTop: 9 }}>
          Divulgation des données
          </Text>
          <Text h6 style={{ paddingTop: 9 }}>
          Conditions légales
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Nkoo peut divulguer vos données personnelles en croyant de bonne foi qu’une telle action est nécessaire pour:       
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          - Pour respecter une obligation légale
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          - Protéger et défendre les droits ou la propriété de Nkoo
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          - Pour prévenir ou étudier les éventuels actes fautifs liés au service
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          - Pour protéger la sécurité personnelle des utilisateurs du service ou du public
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          - Protéger contre toute responsabilité juridique
          </Text>

          <Text h6 style={{ paddingTop: 9 }}>
          Sécurité des données
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          La sécurité de vos données est importante pour nous, mais n’oubliez pas qu’aucune méthode de transmission sur Internet, ni aucune méthode de stockage électronique, n’est sécurisée à 100%. Nous nous efforçons d’utiliser des moyens commercialement acceptables pour protéger vos données personnelles, mais nous ne pouvons pas garantir leur sécurité absolue.  
          </Text>
          <Text h6 style={{ paddingTop: 9 }}>
          Fournisseurs de services
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Nous pouvons faire appel à des sociétés tierces et à des particuliers pour faciliter notre Service (“Fournisseurs de services”), pour fournir le Service en notre nom, pour fournir des services connexes ou pour nous aider à analyser l’utilisation de notre Service.

          Ces tiers n’ont accès à vos données personnelles que pour effectuer ces tâches pour notre compte et sont tenus de ne pas les divulguer ni les utiliser à d’autres fins.
          </Text>
          <Text h6 style={{ paddingTop: 9 }}>
          Analytics
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Nous pouvons utiliser des fournisseurs de service tiers pour contrôler et analyser l’utilisation de notre service.
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Google Analytics est un service d’analyse de site internet proposé par Google qui permet de suivre et de signaler le trafic sur le site Web. Google utilise les données collectées pour suivre et contrôler l’utilisation de notre service. Ces données sont partagées avec d’autres services Google. Google peut utiliser les données collectées pour contextualiser et personnaliser les annonces de son propre réseau de publicité.    

          Vous pouvez choisir de ne pas avoir rendu votre activité sur le service accessible à Google Analytics en installant le module complémentaire de navigateur pour la désactivation de Google Analytics. Ce module empêche le code JavaScript de Google Analytics (ga.js, analytics.js et dc.js) de partager des informations avec Google Analytics sur les visites.

          Pour plus d’informations sur les pratiques de Google en matière de confidentialité, Consultez la page Web Google Privacy & Terms:https://policies.google.com/privacy?hl=fr
          </Text>

          <Text h5 style={{ paddingTop: 9 }}>
            Droit des Utilisateurs
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Conformément à la règlementation, l’Utilisateur bénéficie des droits d’accès et de rectification des données à caractère qui le concerne, qui lui permettent de faire rectifier, compléter, mettre à jour ou effacer les données qui sont inexactes, incomplètes, équivoques, périmées ou dont la collecte, l’utilisation, la communication ou la conservation est interdite.
          </Text>

          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          L’Utilisateur bénéficie également des droits à demander la limitation du traitement, et à s’opposer pour motif légitime au traitement de ses données à caractère personnel. L’Utilisateur peut en outre communiquer des instructions sur le sort de ses données à caractère personnel en cas de décès.
          </Text>

          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Lorsque cela est applicable, l’Utilisateur peut demander la portabilité de ses données, ou, lorsque le traitement a pour base légale le consentement, retirer son consentement à tout moment.
          </Text>

          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          L’Utilisateur peut exercer ses droits en s’adressant par email à privacy@nkoo.io.
          </Text>

          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          L’Utilisateur peut se désinscrire de la newsletter de Nkoo ou des emails marketing en suivant les liens de désinscription figurant dans chacun de ces emails.
          </Text>

          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          L’Utilisateur peut, en cas de contestation, former une réclamation auprès de la CNIL dont les coordonnées figurent à l’adresse internet https://www.cnil.fr.
          </Text>

          <Text h5 style={{ paddingTop: 9 }}>
            Liens vers d’autres sites        
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Notre service peut contenir des liens vers d’autres sites que nous n’exploitons pas. Si vous cliquez sur un lien tiers, vous serez dirigé vers le site de ce tiers. Nous vous conseillons vivement de consulter la politique de confidentialité de chaque site que vous visitez.

          Nous n’exerçons aucun contrôle sur le contenu, les politiques de confidentialité ou les pratiques de tout site ou service tiers, et déclinons toute responsabilité à cet égard.
          </Text>

          <Text h5 style={{ paddingTop: 9 }}>
            Modifications de cette politique de confidentialité 
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Nous pouvons mettre à jour notre politique de confidentialité de temps à autre. Nous vous informerons de tout changement en affichant la nouvelle politique de confidentialité sur cette page.

          Nous vous informerons par e-mail et /ou d’un avis visible sur notre service avant l’entrée en vigueur du changement et mettrons à jour la “date d’entrée en vigueur” en haut de la présente politique de confidentialité.        

          Nous vous conseillons de consulter périodiquement cette politique de confidentialité pour vous tenir informé de toute modification. Les modifications apportées à cette politique de confidentialité entrent en vigueur lorsqu’elles sont publiées sur cette page.
          </Text>

          <Text h5 style={{ paddingTop: 9 }}>
          Contactez-nous 
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
          Si vous avez des questions concernant cette politique de confidentialité, veuillez nous contacter:
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
            - par courrier électronique: contact@nkoo.io
          </Text>
          <Text size={16} style={{ paddingTop: 9 }} color='rgba(0, 0, 0, 0.5)'>
            - en visitant cette page sur notre site web: https://www.nkoo.io/contact/
          </Text>
        </ScrollView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  privacy: {
    padding: theme.SIZES.BASE,
    paddingBottom: theme.SIZES.BASE * 5,
  },
  buttonsWrapper: {
    zIndex: 2,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    paddingHorizontal: theme.SIZES.BASE,
    paddingVertical: theme.SIZES.BASE * 1.75,
  },
  privacyButton: {
    width: '48%',
    height: theme.SIZES.BASE * 3,
    alignItems: 'center',
    shadowColor: "rgba(0, 0, 0, 0.2)",
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 8,
    shadowOpacity: 1
  },
  gradient: {
    zIndex: 1,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: '30%',
  },
});