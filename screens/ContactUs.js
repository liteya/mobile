import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Block, Text, theme } from 'galio-framework';
import { materialTheme } from '../constants/';

export default class About extends React.Component {
  render() {
    return (
      <Block flex>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.about} overScrollMode='always'>
          <Text size={20} center color={materialTheme.COLORS.SECONDARY} bold style={{ paddingTop: 9 }}>
            Des questions ? Des remarques ? Des suggestions ? Un bug ?
          </Text>
          <Text size={16} color='rgba(0, 0, 0, 0.5)' style={{ paddingTop: 9 }}>
            Vous pouvez nous contacter :
          </Text>
          <Text size={16} color='rgba(0, 0, 0, 0.5)' style={{ paddingTop: 9 }}>
            _ par mail : contact@nkoo.io
          </Text>
          <Text size={16} color='rgba(0, 0, 0, 0.5)' style={{ paddingTop: 9 }}>
            _ par téléphone et whatsApp : +33 6 25 51 08 56
          </Text>
          <Text size={16} color='rgba(0, 0, 0, 0.5)' style={{ paddingTop: 9 }}>
            _ via notre site web : www.nkoo.io
          </Text>
          <Text size={16} color='rgba(0, 0, 0, 0.5)' style={{ paddingTop: 9 }}>
            _ via Instagram : https://www.instagram.com/nkoo_academy/
          </Text>
          <Text size={16} color='rgba(0, 0, 0, 0.5)' style={{ paddingTop: 9 }}>
            _ via Facebook : https://www.facebook.com/nkoo.academy
          </Text>
        </ScrollView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  about: {    
    padding: theme.SIZES.BASE,
  },
});
