import React from 'react';
import { StyleSheet, Dimensions, KeyboardAvoidingView, Alert, Platform } from 'react-native';
import { Block, Button, Input, Text, theme } from 'galio-framework';
//import Icon from 'react-native-vector-icons/FontAwesome';
import { FontAwesome } from '@expo/vector-icons';
import { Mutation } from '@apollo/client/react/components';
import { materialTheme, messages } from '../constants/';
import { LOGIN } from "../graphql/mutations";
import Context from '../store/context';
import graphqlErrorsParser from '../utils/graphQLErrorsParser.js';
import validator from 'validator';
import iap from '../store/iap';

const { width, height } = Dimensions.get('window');

export default class SignIn extends React.Component {
  state = {
    email: '',
    password: '',
    active: {
      email: false,
      password: false,
    }
  }

  handleChange = (name, value) => {
    this.setState({ [name]: value });

    if (this.state.errorMessage) {
      this.setState({
        errorMessage: '',
      })
    }
  }

  toggleActive = (name) => {
    const { active } = this.state;
    active[name] = !active[name];

    this.setState({ active });
  }

  storeToken = async (saveToken, token) => {
    saveToken(token)
      .then(() => {
        this.props.navigation.navigate('App');
      })
      .catch(error => {
        this.setState({ error })
      });
  };

  setIAPUserId = async (user) => {
    iap.setUserId(user.id);
    await iap.getActiveProducts();
    await iap.getProductsForSale();
  };

  handleLoginError = async (error) => {
    const graphqlErrors = graphqlErrorsParser(error);

    if (graphqlErrors.displayableError) {
      this.setState({
        errorMessage: graphqlErrors.displayableError,
      })
    }

    if (graphqlErrors.networkError) {
      this.setState({
        errorMessage: messages.ErrorMessageDefault,
      })
    }
  };

  handleLogin = login => {
    if (!validator.isEmail(this.state.email)) {
      this.setState({
        errorMessage: messages.EmailInvalid
      })

      return;
    }

    if (this.state.password.length < 6) {
      this.setState({
        errorMessage: messages.PasswordInvalid
      })

      return;
    }

    login();
  };

  renderErrorMessage = () => {
    if (this.state.errorMessage) {
      return <Text style={{ color: materialTheme.COLORS.ERROR }} size={theme.SIZES.FONT}>{this.state.errorMessage}</Text>;
    }

    return null;
  }

  render() {
    const { navigation } = this.props;

    return (
      <Block flex middle style={[styles.signin, {flex: 1, paddingTop: theme.SIZES.BASE * 2}]}>
        <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} enabled>
            
          {/*<Block middle>
            <Block style={{ marginVertical: theme.SIZES.BASE * 1.5 }}>
              <FontAwesome.Button
                name="facebook"
                backgroundColor={materialTheme.COLORS.FACEBOOK}
                style={{ width: width / 1.2, height: height / 16, justifyContent: 'center' }}
                onPress={() => Alert.alert('Not implemented')}
              >
                {'Connection avec Facebook'.toUpperCase()}
              </FontAwesome.Button>
            </Block>

            <Block>
              <FontAwesome.Button
                name="google"
                backgroundColor={materialTheme.COLORS.GOOGLE}
                style={{ width : width / 1.2, height: height / 16, justifyContent: 'center' }}
                onPress={() => Alert.alert('Not implemented')}
              >
                {'Connection avec Google'.toUpperCase()}
              </FontAwesome.Button>
            </Block>
          </Block>
            
          <Block middle style={{ paddingVertical: theme.SIZES.BASE * 1}}>
            <Text center color={theme.COLORS.INPUT} size={theme.SIZES.FONT * 0.875}>
              ou
            </Text>
          </Block>
          */}
          <Block flex>
            <Button color="transparent" shadowless 
              onPress={() => navigation.navigate('Sign Up')}
            >
              <Text
                center
                bold
                color={materialTheme.COLORS.SECONDARY}
                size={theme.SIZES.FONT}
                style={{textTransform: 'uppercase', textDecorationLine: 'underline'}}
              >
                {"Vous n'avez pas de compte? Cliquez ici pour vous inscrire"}
              </Text>
            </Button>
              <Block center style={{paddingTop: 30}}>
                <Input
                  borderless
                  color="black"
                  placeholder="Veuillez saisir votre email"
                  type="email-address"
                  autoCapitalize="none"
                  bgColor='transparent'
                  onBlur={() => this.toggleActive('email')}
                  onFocus={() => this.toggleActive('email')}
                  placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
                  onChangeText={text => this.handleChange('email', text)}
                  style={[styles.input, this.state.active.email ? styles.inputActive : null]}
                />
                <Input
                  password
                  viewPass
                  borderless
                  color="black"
                  iconColor={theme.COLORS.INPUT}
                  placeholder="Veuillez saisir votre mot de passe"
                  bgColor='transparent'
                  onBlur={() => this.toggleActive('password')}
                  onFocus={() => this.toggleActive('password')}
                  placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
                  onChangeText={text => this.handleChange('password', text)}
                  style={[styles.input, this.state.active.password ? styles.inputActive : null]}
                />
                {this.renderErrorMessage()}
                 {/*<Text
                  color={theme.COLORS.INPUT}
                  size={theme.SIZES.FONT * 0.75}
                  onPress={() => Alert.alert('Not implemented')}
                  style={{ alignSelf: 'flex-end', lineHeight: theme.SIZES.FONT * 2 }}
                >
                  Mot de passe oublié?
                  </Text>*/}
              </Block>
              <Block flex top style={{ marginTop: 20 }}>
                <Context.Consumer>
                  {context => (
                    <Mutation
                      mutation={LOGIN}
                      variables={{ email: this.state.email, password: this.state.password }}
                      onCompleted={data => this.storeToken(context.saveToken, data.login.token) && context.setCurrentUser(data.login.user) && this.setIAPUserId(data.login.user)}
                      onError={error => this.handleLoginError(error)}  
                    >
                      {login => (
                        <Button
                          shadowless
                          uppercase
                          color={this.state.email && this.state.password ? materialTheme.COLORS.BUTTON_COLOR : materialTheme.COLORS.DEFAULT }
                          style={{ height: 48 }}
                          onPress={() => this.handleLogin(login)}
                          disabled={!(this.state.email && this.state.password)}
                        >
                        Je me connecte
                        </Button>
                      )}
                    </Mutation>
                  )}
                </Context.Consumer>

              </Block>
          </Block>
        </KeyboardAvoidingView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  signin: {        
    marginTop: Platform.OS === 'ios' ? 60 : 0,
  },
  input: {
    width: width * 0.9, 
    borderRadius: 0,
    borderBottomWidth: 1,
    borderBottomColor: materialTheme.COLORS.PLACEHOLDER,
  },
  inputActive: {
    borderBottomColor: "black",
  },
});
