import React from 'react';
import { StyleSheet, Dimensions, Image } from 'react-native';
import { Block, Button, Text, theme } from 'galio-framework';

const { height, width } = Dimensions.get('screen');

import materialTheme from '../constants/Theme';
import Images from '../constants/Images';

export default class Onboarding extends React.Component {
  render() {
    const { navigation } = this.props;

    return (
      <Block flex style={styles.container}>
        <Block center style={{ marginVertical: theme.SIZES.BASE }}>
          <Image
            source={{ uri : Images.LogoFull }}
            style={{ height: height / 2.5, width: width / 1.3, zIndex: 1 }}
          />
          <Text
            center
            color="black"
            h5
          >
            Votre partenaire qui vous aide à apprendre et parler une langue africaine !
          </Text>
        </Block>
        <Block flex={1.3} space="between" style={styles.padded}>
          <Block center>
            <Button
              shadowless
              uppercase
              style={styles.button}
              color={materialTheme.COLORS.BUTTON_COLOR}
              onPress={() => navigation.navigate('Sign In')}>
              Se connecter
            </Button>
          </Block>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.COLORS.WHITE,
  },
  padded: {
    // paddingHorizontal: theme.SIZES.BASE * 2,
    marginTop: 80, 
    position: 'relative',
    bottom: Platform.OS === 'ios' ? theme.SIZES.BASE : theme.SIZES.BASE * 4,
  },
  button: {
    width: width - theme.SIZES.BASE * 4,
    height: theme.SIZES.BASE * 3,
    shadowRadius: 0,
    shadowOpacity: 0,
  },
  pro: {
    backgroundColor: materialTheme.COLORS.LABEL,
    paddingHorizontal: 8,
    marginLeft: 12,
    borderRadius: 2,
    height: 22
  },
  gradient: {
    zIndex: 1,
    position: 'absolute',
    top: 33 + theme.SIZES.BASE,
    left: 0,
    right: 0,
    height: 66,
  },
});
