import React from 'react';
import { StyleSheet, Dimensions, ScrollView } from 'react-native';
import { Block, theme } from 'galio-framework';
import { Course } from '../components';
import Context from '../store/context';

const { width } = Dimensions.get('screen');

const DEFAULT_LEVEL = 0;
const COURSE_COMPLETED = 'COMPLETED';
const COURSE_OPEN = 'OPEN';

export default class CoursesList extends React.Component {

  getCourseStatus(course, chapter, currentLanguage) {
    const isCourseBelongsToChapterCompleted = currentLanguage.levels[DEFAULT_LEVEL].chapters.completed.find(elt => elt.id === chapter.id);
    if (isCourseBelongsToChapterCompleted) {
      return COURSE_COMPLETED;
    }

    const isCourseCompleted = currentLanguage.levels[DEFAULT_LEVEL].chapters.current.courses.completed.find(elt => elt.id === course.id.id);
    if (isCourseCompleted) {
      return COURSE_COMPLETED;
    }

    return COURSE_OPEN;
  }

  render() {
    const { route } = this.props;
    const chapterCourses = route.params.chapterCourses;

    return (
      <Block flex center style={styles.home}>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.courses}>
          <Context.Consumer>
            {context => {
              if (context.getCurrentUser()) {
                const currentLanguage = context.getCurrentUser().languages.find(language => language.isCurrentLearning === true);
                return (<Block flex>
                  {chapterCourses
                    .sort((a, b) => a.order - b.order)
                    .map(course => <Course key={course.id.id} courseId={course.id.id} status={this.getCourseStatus(course, context.getChapter(), currentLanguage)} />)}
                </Block>)
              }
            }}
          </Context.Consumer>
        </ScrollView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,    
  },
  courses: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE * 2,
  },
});
