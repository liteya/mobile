import React from 'react';
import { ScrollView, StyleSheet, Dimensions, ActivityIndicator } from 'react-native';
import { Block, Text, theme } from 'galio-framework';
import { Images, materialTheme } from '../constants';
import { FontAwesome } from '@expo/vector-icons';
import { List, Avatar } from 'react-native-paper';
import { ALL_LANGUAGES } from "../graphql/queries";
import { ADD_USER_LANGUAGE } from "../graphql/mutations";
import { Query } from '@apollo/client/react/components';
import Context from '../store/context';
import { Mutation } from '@apollo/client/react/components';

const { width, height } = Dimensions.get('screen');

export default class AvailableCoursesList extends React.Component {

  render() {
    const { navigation } = this.props;
    //const [addTodo, { data }] = useMutation(ADD_TODO);

    return (
      <Block flex>
        <List.Section>
          <Query
            query={ALL_LANGUAGES}
            variables={{ filter: {"activated" : true} }}
          >
            {({ loading, error, data }) => {
              if (loading || error) return <ActivityIndicator size="large" color={materialTheme.COLORS.PRIMARY} />
       
              return (
                data.allLanguages.filter(l => l.activated).map((l) => (
                  <List.Accordion
                    key= { l.id }
                    title={ l.title }
                    titleStyle={ styles.btn }
                    //left={props => <Avatar.Image size={45} source={Images.IconDefault} />}
                  >
                    <Context.Consumer>
                      {context => (
                        <Mutation
                          mutation={ADD_USER_LANGUAGE}
                          variables={{ userId: context.getCurrentUser().id, languageId: l.id }}
                          onCompleted={data => context.setCurrentUser(data.addUserLanguage)}
                          //onError={error => this.handleLoginError(error)}  
                        >
                          {addUserLanguage => (
                            <Block center style={{marginLeft:10, marginRight:10}}>
                              <Text style={{ marginBottom: 10 }}>{ l.description }</Text>
                              <FontAwesome.Button
                                name="book"
                                size={20}
                                backgroundColor={materialTheme.COLORS.SECONDARY}
                                style={{ width : width / 1.4, height: height / 19, justifyContent: 'center' }}
                                onPress={() => addUserLanguage() && navigation.navigate('Home')}
                              >
                                {'Commencer à apprendre'.toUpperCase()}
                              </FontAwesome.Button>
                            </Block>
                          )}
                        </Mutation>
                      )}    
                    </Context.Consumer>
                  </List.Accordion>
                ))
              )
            }}
          </Query>
        </List.Section>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  btn: {    
    color: 'black',
  }
});
