import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Context from './context';

export default class ContextProvider extends React.Component {
  state = {
    // Token
    token: '',
    saveToken: async (token) => {
      try {
        this.setState({ token });
        const resp = await AsyncStorage.setItem('userToken', token);
        return resp;
      }
      catch (error) {
        this.setState({ error })
      }
    },
    removeToken: async () => {
      try {
        this.setState({ token : null });
        await AsyncStorage.removeItem('userToken');
      }
      catch (error) {
        this.setState({ error })
      }
    },
    getToken: async () => {
      try {
        const resp = await AsyncStorage.getItem('userToken');
        return resp;
      }
      catch (error) {
        this.setState({ error })
      }
    },
    // Rubric
    chapter: null,
    setChapter: (chapter) => {
      this.setState({ chapter });
    },
    getChapter: () => this.state.chapter,

    // Course
    course: null,
    setCourse: (course) => {
      this.setState({ course });
      this.state.flushWrongAnswerQuestionList();
    },
    getCourse: () => this.state.course,

    // current user
    currentUser: null,
    setCurrentUser: async (currentUser) => {
      this.setState({ currentUser : currentUser });
      const resp = await AsyncStorage.setItem('currentUser', JSON.stringify(currentUser));
      return resp;
    },
    getCurrentUser: () => this.state.currentUser,
    removeCurrentUser: async () => {
      try {
        await AsyncStorage.removeItem('currentUser');
        this.setState({ currentUser : null }); 
      }
      catch (error) {
        this.setState({ error })
      }
    },

    // CurrentQuestionOrder
    currentQuestionOrder: null,
    setCurrentQuestionOrder: (currentQuestionOrder) => {
      this.setState({ currentQuestionOrder });
    },
    getCurrentQuestionOrder: () => this.state.currentQuestionOrder,

    // WrongAnswerQuestionList
    wrongAnswerQuestionList: [],
    getWrongAnswerQuestionList: () => this.state.wrongAnswerQuestionList,
    addWrongAnswerQuestionList: () => {
      this.setState(state => {
        state.wrongAnswerQuestionList.unshift(this.state.currentQuestionOrder);
        
        return {
          wrongAnswerQuestionList: state.wrongAnswerQuestionList
        };
      });
    },
    getNextWrongAnswerQuestionOrder: () => { 
      const questionOrder = this.state.wrongAnswerQuestionList[this.state.wrongAnswerQuestionList.length - 1];

      return questionOrder;
    },
    flushWrongAnswerQuestionList: () => {
      this.setState({
        wrongAnswerQuestionList: [],
        isQuestionFromWrongAnswerQuestionList: false
      });
    },
    popWrongAnswerQuestionList: () => {
      this.setState(state => {
        state.wrongAnswerQuestionList.pop();
        return {
          wrongAnswerQuestionList: state.wrongAnswerQuestionList
        };
      });
    },
    // IsQuestionFromWrongAnswerQuestionList
    isQuestionFromWrongAnswerQuestionList: false,
    setIsQuestionFromWrongAnswerQuestionList: (isQuestionFromWrongAnswerQuestionList) => {
      this.setState({
        isQuestionFromWrongAnswerQuestionList
      });
    },
    getIsQuestionFromWrongAnswerQuestionList: () => this.state.isQuestionFromWrongAnswerQuestionList,
  }

  componentDidMount() {
    AsyncStorage.getItem('userToken')
      .then((token) => {
        this.setState({ token });
      })
      .catch(error => {
        this.setState({ error });
      });

      AsyncStorage.getItem('currentUser')
      .then((currentUser) => {
        this.setState({ currentUser : JSON.parse(currentUser) });
      })
      .catch(error => {
        this.setState({ error });
      });
  }

  render() {
    return (
      <Context.Provider value={this.state}>
        {this.props.children}
      </Context.Provider>
    );
  }
}