import { Alert } from 'react-native';
import Iaphub from 'react-native-iaphub';
import pkg from '../package.json';

class IAPStore {

	isInitialized = false;
	skuProcessing = null;
	productsForSale = null;
	activeProducts = null;

	// Init IAPHUB
	async init() {
		try {
			// Init iaphub
      await Iaphub.init({
        appId: "60500487051e8e3d46eecea4",
        apiKey: "PD556XOeLBrcQq0kiJkKK5EooQIBsA3",
        environment: "production"
      });

			// Add device params
			Iaphub.setDeviceParams({appVersion: pkg.version});
			// Iaphub is now initialized and ready to use
			this.isInitialized = true;
		} catch (err) {
			console.error('---------------- IAPHUB init error --------------- ', err);
			// The init has failed (the error code is available in the 'err.code' property)
			// You probably forgot to specify an option (appId, apiKey...)
			// Or the user is not allowed to make payments, IOS only (Error code: 'billing_disabled')
			// Or the billing system is unavailable, it may be a problem with the device or Itunes/Play Store is down (Error code: 'billing_unavailable')
			// Or it is an unknown error, probably the native library of react-native-iap that is not installed properly (Error code: 'billing_error')
		}
	}

	// Set user id
	async setUserId(userId) {
		Iaphub.setUserId(userId);
	}

	// Get products for sale
	async getProductsForSale() {
		this.productsForSale = await Iaphub.getProductsForSale();
	}

	// Get active products
	async getActiveProducts() {
		this.activeProducts = await Iaphub.getActiveProducts();
	}

	// Call this method when an user click on one of your products
	async buy(productSku) {
		try {
			this.skuProcessing = productSku;
			var transaction = await Iaphub.buy(productSku, {onReceiptProcess: () => {
        console.log('-> Processing receipt')
      }});

			this.skuProcessing = null;
			// The webhook could not been sent to my server
			if (transaction.webhookStatus == "failed") {
				Alert.alert(
					"Achat retardé",
					"Votre achat est en cours de traitement, nous avons besoin de plus de temps pour le valider. Vous serez notifier dès que la formule premium sera activée sur votre compte !"
				);
			}
			// Everything was successful! Yay!
			else {
				Alert.alert(
					"Achat réussi",
					"Votre achat a été traité avec succès ! Profitez maintenant de la formule premium !"
				);
			}
			// Refresh the user to update the products for sale
			try {
				await this.getActiveProducts();
				await this.getProductsForSale();
			} catch (err) {
				console.error('---------------- IAPStore refresh after buy error -------------- ', err);
			}

		} catch (err) {
      console.info('---------------- IAPStore transaction buy error -------------- ', err.code);

			this.skuProcessing = null;
			// Purchase popup cancelled by the user (ios only)
			if (err.code == "user_cancelled") return
			// Couldn't buy product because it has been bought in the past but hasn't been consumed (restore needed)
			else if (err.code == "product_already_owned") {
				Alert.alert(
					"Vous êtes déjà abonné à la formule premium",
					"Veuillez restaurer votre achat afin de corriger ce problème.",
					[
						{text: 'Annuler', style: 'cancel'},
						{text: 'Restaurer', onPress: () => Iaphub.restore()}
					]
				);
			}
			// The payment has been deferred (its final status is pending external action such as 'Ask to Buy')
			else if (err.code == "deferred_payment") {
				Alert.alert(
					"Achat en attente de confirmation",
					"Votre achat a été traité mais il est en attente de confirmation."
				);
			}
			// The receipt has been processed on IAPHUB but something went wrong
			else if (err.code == "receipt_validation_failed") {
				Alert.alert(
					"Nous rencontrons des difficultés pour valider votre transaction",
					"Donnez-nous un peu de temps, nous réessayerons de valider votre transaction dès que possible ou contacter nous à contact@nkoo.io ou par WhatsApp/téléphone au 0033 6 25 51 08 56."
				);
			}
			// The receipt hasn't been validated on IAPHUB (Could be an issue like a network error...)
			else if (err.code == "receipt_request_failed") {
				Alert.alert(
					"Nous rencontrons des difficultés pour valider votre transaction",
					"Veuillez restaurer votre abonnement dans l'onglet Paramètres ou contacter nous à contact@nkoo.io ou par WhatsApp/téléphone au 0033 6 25 51 08 56."
				);
			}
			// The user has already an active subscription on a different platform (android or ios)
			else if (err.code == "cross_platform_conflict") {
				Alert.alert(
					`Il semblerait que vous avez déjà un abonnement sur ${err.params.platform}`,
					`Veuillez contacter le service client à serviceclient@nkoo.io ou par WhatsApp/téléphone au 0033 6 25 51 08 56.`
				);
			}
			// Couldn't buy product for many other reasons (the user shouldn't be charged)
			else {
				Alert.alert(
					"Purchase error",
					"Nous ne sommes pas en mesure de traiter votre achat, veuillez nous contacter à contact@nkoo.io ou par WhatsApp/téléphone au 0033 6 25 51 08 56."
				);
			}
		}
	}

	// Call this method to restore the user purchases (you should have a button, it is usually displayed on the settings page)
	async restore() {
		await Iaphub.restore();
		await Iaphub.getActiveProducts();
		await Iaphub.getProductsForSale();
		Alert.alert("Restaurer", "Votre abonnement a été restauré.");
	}
}

export default new IAPStore();