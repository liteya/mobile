<script src="http://localhost:8097"></script>
import RNUxcam from 'react-native-ux-cam';
import React from 'react';
import { Platform, StatusBar, Image } from 'react-native';
import AppLoading from 'expo-app-loading';
import { Asset } from 'expo-asset';
import { Block, GalioProvider } from 'galio-framework';
import { NavigationContainer } from '@react-navigation/native';
import { ApolloProvider } from '@apollo/client';
import { client } from './graphql/Client'
import ContextProvider from './store/ContextProvider';
import * as Font from 'expo-font';
import { MaterialIcons, Ionicons, MaterialCommunityIcons, Foundation, FontAwesome } from '@expo/vector-icons'
import { Query } from '@apollo/client/react/components';
// Before rendering any navigation stack
import { enableScreens } from 'react-native-screens';
import WonderPush from 'react-native-wonderpush';
enableScreens();

import Screens from './navigation/Screens';
import { Images, materialTheme } from './constants';
import iap from './store/iap';
import Context from './store/context';
import { GET_ONE_USER } from "./graphql/queries";
import { Alert } from 'react-native';


const assetImages = [
];

// cache product images
// products.map(product => assetImages.push(product.image));

// cache categories images
// Object.keys(categories).map(key => {
//   categories[key].map(category => assetImages.push(category.image));
// });

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}
export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };

  setIAPUserId = async (user) => {
    iap.setUserId(user.id);
    await iap.getActiveProducts();
    await iap.getProductsForSale();

    RNUxcam.optIntoSchematicRecordings();
    RNUxcam.startWithKey('2cyz9dyq5idjze1');
    RNUxcam.setUserIdentity(user.id);

    if (user.id) {
      WonderPush.subscribeToNotifications();
      WonderPush.setUserId(user.id);
    }
  };

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._startAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <ApolloProvider client={client}>
          <ContextProvider>
            <Context.Consumer>
              {context => {
                const currentUser = context.getCurrentUser();
                return (
                  <NavigationContainer>
                    <GalioProvider theme={materialTheme}>
                      {currentUser 
                        ? <Query
                            query={GET_ONE_USER}
                            variables={{ id: currentUser.id }}
                            onCompleted={data => context.setCurrentUser(data.User) && this.setIAPUserId(data.User)}
                            fetchPolicy={'no-cache'}
                          >
                          {() => {
                            return (
                              <Block flex>
                                {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                                <Screens />
                              </Block>
                            )
                          }}
                          </Query> 
                        : <Block flex>
                            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                            <Screens />
                          </Block>
                    }
                    </GalioProvider>
                  </NavigationContainer>
                )
              }}
            </Context.Consumer>
          </ContextProvider>
        </ApolloProvider>
      );
    }
  }

  _startAsync = async () => {
    await iap.init();

    return this._loadResourcesAsync();
  };

  _loadResourcesAsync = async () => {
    return Promise.all([
      Font.loadAsync({
        ...Ionicons.font,
        ...MaterialCommunityIcons.font,
        ...MaterialIcons.font,
        ...Foundation.font,
        ...FontAwesome.font,
      }),
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}
