import gql from 'graphql-tag';

const SIGN_UP = gql`
  mutation Signup($input: UserInput!) {
    signup(input: $input) {
      token,
      user {
        id,
        email,
        name,
        premiumAccess {
          activated,
          formula
        },
        createdAt,
        languages {
          id {
            id,
            title
          },
          isCurrentLearning,
          levels {
            id {
              id,
              title
            }
            chapters {
              completed {
                id,
                title,
                __typename
              }
              current {
                id {
                  id,
                  title,
                  __typename
                }
                courses {
                  completed {
                    id,
                    title,
                    __typename
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
const LOGIN = gql`
  mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token,
      user {
        id,
        email,
        name,
        premiumAccess {
          activated,
          formula
        },
        createdAt,
        languages {
          id {
            id,
            title
          },
          isCurrentLearning,
          levels {
            id {
              id,
              title
            }
            chapters {
              completed {
                id,
                title,
                __typename
              }
              current {
                id {
                  id,
                  title,
                  __typename
                }
                courses {
                  completed {
                    id,
                    title,
                    __typename
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`

const ADD_USER_LANGUAGE = gql`
  mutation AddUserLanguage($userId: ID!, $languageId: ID!) {
    addUserLanguage(userId: $userId, languageId: $languageId) {
      id,
      email,
      name,
      premiumAccess {
        activated,
        formula
      },
      createdAt,
      languages {
        id {
          id,
          title
        },
        isCurrentLearning,
        levels {
          id {
            id,
            title
          }
          chapters {
            completed {
              id,
              title,
              __typename
            }
             current {
              id {
                id,
                title,
                __typename
              }
               courses {
                completed {
                  id,
                  title,
                  __typename
                }
              }
            }
          }
        }
      }
    }
  }
`

const ACTIVATE_CURRENT_LEARNING_LANGUAGE = gql`
  mutation ActivateCurrentLearningLanguage($userId: ID!, $languageId: ID!) {
    activateCurrentLearningLanguage(userId: $userId, languageId: $languageId) {
      id,
      email,
      name,
      premiumAccess {
        activated,
        formula
      },
      createdAt,
      languages {
        id {
          id,
          title
        },
        isCurrentLearning,
        levels {
          id {
            id,
            title
          }
          chapters {
            completed {
              id,
              title,
              __typename
            }
             current {
              id {
                id,
                title,
                __typename
              }
               courses {
                completed {
                  id,
                  title,
                  __typename
                }
              }
            }
          }
        }
      }
    }
  }
`

const UPDATE_USER = gql`
  mutation UpdateUser($id: ID!, $name: String, $password: String, $email: String) {
    updateUser(id: $id, name: $name, password: $password, email: $email) {
      id,
      email,
      name,
      premiumAccess {
        activated,
        formula
      },
      createdAt,
      languages {
        id {
          id,
          title,
          __typename
        },
        isCurrentLearning,
        levels {
          id {
            id,
            title,
            __typename
          }
          chapters {
            completed {
              id,
              title,
              __typename
            }
             current {
              id {
                id,
                title,
                __typename
              }
               courses {
                completed {
                  id,
                  title,
                  __typename
                }
              }
            }
          }
        }
      }
    }
  }
`

const ADD_COURSE_COMPLETED = gql`
  mutation AddCourseCompleted($userId: ID!, $courseId: ID!, $chapterId: ID!) {
    addCourseCompleted(userId: $userId, courseId: $courseId, chapterId: $chapterId) {
      id,
      email,
      name,
      premiumAccess {
        activated,
        formula
      },
      createdAt,
      languages {
        id {
          id,
          title
        },
        isCurrentLearning,
        levels {
          id {
            id,
            title
          }
          chapters {
            completed {
              id,
              title,
              __typename
            }
            current {
              id {
                id,
                title,
                __typename
              }
              courses {
                completed {
                  id,
                  title,
                  __typename
                }
              }
            }
          }
        }
      }
    }
  }
`

export {
  SIGN_UP,
  LOGIN,
  ADD_USER_LANGUAGE,
  UPDATE_USER,
  ACTIVATE_CURRENT_LEARNING_LANGUAGE,
  ADD_COURSE_COMPLETED
}