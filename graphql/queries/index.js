import gql from 'graphql-tag';

const ALL_LANGUAGES = gql`
  query AllLanguages($filter: LanguageFilter) {
    allLanguages(filter: $filter) {
      id,
      title,
      description,
      activated,
      levels {
        id {
          id,
          title,
          description
        }
      }
    }
  }
`

const ALL_CHAPTERS = gql`
  query AllChapters($page: Int, $perPage: Int, $sortField: String, $sortOrder: String, $filter: ChapterFilter) {
    allChapters(page: $page, perPage: $perPage, sortField: $sortField, sortOrder: $sortOrder, filter: $filter) {
      id,
      title,
      description,
      image,
      premiumAccess,
      activated,
      level {
        id
        title
      },
      courses {
        id {
          title,
          description,
        },
        order
      },
    }
  }
`
const GET_ONE_LANGUAGE = gql`
  query Language($id: ID!) {
    language(id: $id)  {
      id,
      title,
      description,
      activated,
      levels {
        id {
          id,
          title,
          description,
          chapters {
            id {
              id,
              title,
              description,
              image,
              premiumAccess,
              activated,
              courses {
                id {
                  id,
                  title,
                  description
                  questions {
                    id {
                      id,
                      type,
                      content
                    },
                    order
                  }
                },
                order
              }
            },
            order
          }
        },
        order
      }
    }
  }
`
const GET_ONE_USER = gql`
query User($id: ID!) {
  User(id: $id)  {
    id,
    email,
    name,
    premiumAccess {
      activated,
      formula
    },
    createdAt,
    languages {
      id {
        id,
        title
      },
      isCurrentLearning,
      levels {
        id {
          id,
          title
        }
        chapters {
          completed {
            id,
            title,
            __typename
          }
          current {
            id {
              id,
              title,
              __typename
            }
            courses {
              completed {
                id,
                title,
                __typename
              }
            }
          }
        }
      }
    }
  }
}
`

export {
  ALL_LANGUAGES,
  ALL_CHAPTERS,
  GET_ONE_LANGUAGE,
  GET_ONE_USER
}