import { ApolloClient, InMemoryCache, createHttpLink, from } from '@apollo/client';
import getEnvVars from '../environment';
const { apiUrl } = getEnvVars;

const httpLink = createHttpLink({
  uri: apiUrl,
});

export const client = new ApolloClient({
  link: from([httpLink]),
  cache: new InMemoryCache(),
});